module gitlab.com/bichon-project/bichon

go 1.13

require (
	github.com/alecthomas/participle/v2 v2.0.0-alpha3
	github.com/casimir/xdg-go v0.0.0-20160329195404-372ccc2180da
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
	github.com/gdamore/tcell/v2 v2.2.0
	github.com/go-ini/ini v1.62.0
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5
	github.com/xanzy/go-gitlab v0.47.0
	github.com/zalando/go-keyring v0.1.1
	gitlab.com/bichon-project/tview v0.0.0-20210315145417-988de25037a9
	golang.org/x/crypto v0.0.0-20210314154223-e6e6c4f2bb5b
	gopkg.in/src-d/go-git.v4 v4.13.1
)
