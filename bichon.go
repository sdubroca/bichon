// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/url"
	"os"
	"runtime/pprof"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/pflag"

	"gitlab.com/bichon-project/bichon/net"
	"gitlab.com/bichon-project/bichon/security"
	"gitlab.com/bichon-project/bichon/view"
)

func main() {
	var cacerts string
	var httpproxy string
	var httplogdir string
	var logfile string
	var profilecpu string
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.CommandLine.StringVar(&cacerts, "ca-certs", "",
		"path to PEM file containing extra CA certificates to trust")
	pflag.CommandLine.StringVar(&httpproxy, "http-proxy", "",
		"HTTP proxy URL (default: $HTTP_PROXY env variable)")
	pflag.CommandLine.StringVar(&httplogdir, "http-log-dir", "",
		"path to directory to store HTTP request/response logs")
	pflag.CommandLine.StringVar(&logfile, "log-file", "",
		"file name to write debugging logs to")
	pflag.CommandLine.StringVar(&profilecpu, "profile-cpu", "",
		"file name to write CPU profiling data to")
	pflag.Parse()

	var logoutput *os.File
	if logfile != "" {
		var err error
		logoutput, err = os.OpenFile(logfile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			fmt.Printf("Cannot create logfile %s: %s", logfile, err)
			return
		}
		defer logoutput.Close()
		log.SetOutput(logoutput)
		log.SetFormatter(&log.TextFormatter{})
	} else {
		log.SetOutput(ioutil.Discard)
	}

	if profilecpu != "" {
		f, err := os.Create(profilecpu)
		if err != nil {
			fmt.Printf("Cannot create CPU profile data %s: %s", profilecpu, err)

			return
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	rand.Seed(time.Now().UnixNano())

	var tlscfg *tls.Config
	var httpproxyurl *url.URL
	var err error
	if cacerts != "" {
		tlscfg, err = security.LoadCACerts(cacerts)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to load CA certs from %s: %s\n", cacerts, err)
			return
		}
	}
	if httpproxy != "" {
		httpproxyurl, err = url.Parse(httpproxy)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to parse proxy URL %s: %s\n", httpproxy, err)
			return
		}
	}

	net.SetHTTPLogDir(httplogdir)

	display, err := view.NewDisplay(tlscfg, httpproxyurl)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to create display: %s\n", err)
		return
	}

	display.Run()
}
