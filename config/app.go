// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019-2020 Red Hat, Inc.

package config

import (
	"encoding/base64"
	"fmt"
	"os"

	"github.com/go-ini/ini"

	"gitlab.com/bichon-project/bichon/config/xdg"
	"gitlab.com/bichon-project/bichon/security"
)

type KeyBackend string

var (
	KeyBackendUndefined = KeyBackend("")
	KeyBackendKeyring   = KeyBackend("keyring")
	KeyBackendArgon2    = KeyBackend("argon2")
)

type AppConfig struct {
	Projects  AppConfigProjects
	Interface AppConfigInterface
}

type AppConfigProjects struct {
	TokenMasterKey KeyBackend
	Argon2Params   *security.MasterKeyArgon2Params
}

type AppConfigInterface struct {
	DontConfirmExit bool
}

func SaveAppConfig(app *AppConfig) error {
	apppath := xdg.ConfigPath("app.ini")

	cfg := ini.Empty()

	sec, _ := cfg.NewSection("projects")

	if app.Projects.TokenMasterKey != KeyBackendUndefined {
		key, _ := sec.NewKey("token-master-key", string(app.Projects.TokenMasterKey))
		key.Comment = "CAUTION: any changes made to this setting will make it\n" +
			"impossible to decrypt existing stored API tokens in projects.ini"
	}

	if app.Projects.TokenMasterKey == KeyBackendArgon2 &&
		app.Projects.Argon2Params != nil {
		params := app.Projects.Argon2Params
		sec, _ = cfg.NewSection("projects-argon2-params")

		sec.Comment = "CAUTION: any changes made to settings in this group will make it\n" +
			"impossible to decrypt existing stored API tokens in projects.ini"

		salt := base64.StdEncoding.EncodeToString(params.Salt)
		sec.NewKey("salt", salt)
		sec.NewKey("time", fmt.Sprintf("%d", params.Time))
		sec.NewKey("memory", fmt.Sprintf("%d", params.Memory))

		digest := base64.StdEncoding.EncodeToString(params.Digest)
		sec.NewKey("digest", digest)
		salt = base64.StdEncoding.EncodeToString(params.DigestSalt)
		sec.NewKey("digest-salt", salt)
		sec.NewKey("digest-time", fmt.Sprintf("%d", params.Time))
		sec.NewKey("digest-memory", fmt.Sprintf("%d", params.Memory))
	}

	sec, _ = cfg.NewSection("interface")

	if app.Interface.DontConfirmExit {
		sec.NewKey("dont-confirm-exit", "true")
	} else {
		sec.NewKey("dont-confirm-exit", "false")
	}

	return AtomicSave(cfg, apppath)
}

func LoadAppConfig() (*AppConfig, error) {
	app := &AppConfig{}

	apppath := xdg.ConfigPath("app.ini")

	cfg, err := ini.Load(apppath)
	if err != nil {
		if os.IsNotExist(err) {
			return app, nil
		}
		return nil, err
	}

	sec := cfg.Section("projects")

	app.Projects.TokenMasterKey = KeyBackend(sec.Key("token-master-key").String())

	if app.Projects.TokenMasterKey == KeyBackendArgon2 {
		sec, _ = cfg.GetSection("projects-argon2-params")

		if sec != nil {
			params := &security.MasterKeyArgon2Params{}

			salt64 := sec.Key("salt").String()
			salt, _ := base64.StdEncoding.DecodeString(salt64)
			params.Salt = salt

			params.Time = uint32(sec.Key("time").MustInt())
			params.Memory = uint32(sec.Key("memory").MustInt())

			digest64 := sec.Key("digest").String()
			digest, _ := base64.StdEncoding.DecodeString(digest64)
			params.Digest = digest

			salt64 = sec.Key("digest-salt").String()
			salt, _ = base64.StdEncoding.DecodeString(salt64)
			params.DigestSalt = salt

			params.DigestTime = uint32(sec.Key("digest-time").MustInt())
			params.DigestMemory = uint32(sec.Key("digest-memory").MustInt())

			app.Projects.Argon2Params = params
		}
	}

	sec = cfg.Section("interface")
	app.Interface.DontConfirmExit = sec.Key("dont-confirm-exit").MustBool()

	return app, nil
}
