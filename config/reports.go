// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package config

import (
	"os"

	"github.com/go-ini/ini"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config/xdg"
	"gitlab.com/bichon-project/bichon/model"
)

func SaveReports(reports []model.Report) error {
	reportspath := xdg.ConfigPath("reports.ini")

	cfg := ini.Empty()

	for _, report := range reports {
		sec, _ := cfg.NewSection(report.ID)

		sec.NewKey("name", report.Name)
		sec.NewKey("filter", report.Filter)
		sec.NewKey("sorter", report.Sorter)
		if report.Default {
			sec.NewKey("default", "true")
		}
	}

	return AtomicSave(cfg, reportspath)
}

func DefaultReports() []model.Report {
	return []model.Report{
		model.NewReport("All open merge requests",
			"~s open", "age", true),
	}
}

func EnsureReports() ([]model.Report, error) {
	reports := DefaultReports()
	err := SaveReports(reports)
	if err != nil {
		return []model.Report{}, nil
	}
	return reports, nil
}

func LoadReports() ([]model.Report, error) {
	reportspath := xdg.ConfigPath("reports.ini")

	cfg, err := ini.Load(reportspath)
	if err != nil {
		if os.IsNotExist(err) {
			return EnsureReports()
		}
		return []model.Report{}, err
	}

	log.Infof("Loading reports from %s", reportspath)
	var reports []model.Report
	hasDefault := false
	for _, sec := range cfg.Sections() {
		if sec.Name() == ini.DefaultSection {
			continue
		}

		id := sec.Name()
		name := sec.Key("name").String()
		filter := sec.Key("filter").String()
		sorter := sec.Key("sorter").String()
		def := false
		if sec.HasKey("default") && !hasDefault {
			def = sec.Key("default").MustBool()
			hasDefault = def
		}

		report := model.Report{
			ID:      id,
			Name:    name,
			Filter:  filter,
			Sorter:  sorter,
			Default: def,
		}

		reports = append(reports, report)
	}

	if len(reports) == 0 {
		return EnsureReports()
	}

	if !hasDefault {
		reports[0].Default = true
	}

	return reports, nil
}
