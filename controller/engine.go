// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package controller

import (
	"crypto/tls"
	"fmt"
	"net/url"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/cache"
	"gitlab.com/bichon-project/bichon/config/xdg"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
)

type Engine interface {
	AddRepository(repo model.Repo)
	UpdateRepository(repo model.Repo)
	RemoveRepository(repo model.Repo)

	RefreshRepos()
	RefreshMergeRequest(mreq model.MergeReq)

	LoadMergeRequestSeriesPatches(mreq model.MergeReq, series model.Series)
	LoadMergeRequestCommitDiffs(mreq model.MergeReq, commit model.Commit)

	AddMergeRequestThread(mreq model.MergeReq, text string, standalone bool, context *model.CommentContext)
	AddMergeRequestReply(mreq model.MergeReq, thread, text string)

	MarkRead(mreq model.MergeReq)

	ResolveMergeRequestThread(mreq model.MergeReq, thread string, resolved bool)

	AcceptMergeRequest(mreq model.MergeReq)
	ApproveMergeRequest(mreq model.MergeReq)
	UnapproveMergeRequest(mreq model.MergeReq)
}

type engineImplRepo struct {
	Repo model.Repo
	// Once added the MergeReq should be treated as immutable
	// To change it, create a copy, change the copy and assign
	// that copy back into this map
	MergeRequests map[uint]*model.MergeReq
	Removed       bool
	Source        source.Source
	RefreshedAt   time.Time
}

type engineImpl struct {
	// Immutable
	RefreshInterval time.Duration
	TLSConfig       *tls.Config
	HTTPProxy       *url.URL
	Listener        Listener

	// Only access from updateLoop goroutine
	Cache cache.Cache
	Repos []engineImplRepo

	// For comms with updateLoop goroutine
	RefreshTimer   *time.Timer
	PriorityJobs   chan engineJob
	BackgroundJobs chan engineJob
}

func NewEngine(listener Listener, tlscfg *tls.Config, httpproxy *url.URL) Engine {
	mreqcache := cache.NewFileCache(xdg.CachePath("merge-requests"))

	engine := &engineImpl{
		RefreshInterval: time.Minute * 15,
		TLSConfig:       tlscfg,
		HTTPProxy:       httpproxy,

		Listener: listener,

		Cache: mreqcache,

		RefreshTimer:   time.NewTimer(0), // Immediate expire first time
		PriorityJobs:   make(chan engineJob, 1000),
		BackgroundJobs: make(chan engineJob, 1000),
	}

	go engine.updateLoop()

	return engine
}

func (engine *engineImpl) loadMergeRequestCache(enginerepo *engineImplRepo) {
	engine.Listener.Status(fmt.Sprintf("Loading merge request cache"))
	mreqids, _ := engine.Cache.ListMergeRequests(&enginerepo.Repo)
	engine.Listener.Status(fmt.Sprintf("Loading %d merge requests from cache", len(mreqids)))

	for idx, id := range mreqids {
		engine.Listener.Status(fmt.Sprintf("Loading merge request %d from cache [%d/%d]",
			id, idx+1, len(mreqids)))
		mreq, err := engine.Cache.LoadMergeRequest(&enginerepo.Repo, id)
		if err != nil {
			continue
		}

		enginerepo.MergeRequests[mreq.ID] = mreq
		engine.Listener.MergeRequestNotify(mreq)
	}
	engine.Listener.Status("")
}

func (engine *engineImpl) cloneMergeRequest(enginerepo *engineImplRepo, id uint) (model.MergeReq, bool) {
	mreq, ok := enginerepo.MergeRequests[id]
	if !ok {
		engine.Listener.Status(fmt.Sprintf("Missing merge request %d", id))
		return model.MergeReq{}, false
	}

	return *mreq, true
}

func (engine *engineImpl) replaceMergeRequest(enginerepo *engineImplRepo, mreq model.MergeReq) {
	enginerepo.MergeRequests[mreq.ID] = &mreq
	engine.Listener.MergeRequestNotify(&mreq)

	err := engine.Cache.SaveMergeRequest(&mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s", mreq.ID, err))
	} else {
		engine.Listener.Status("")
	}
}

func (engine *engineImpl) refreshMergeRequest(enginerepo *engineImplRepo, mreq *model.MergeReq) {
	versions, err := enginerepo.Source.GetVersions(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request versions: %s", err))
		return
	}

	for idx, _ := range versions {
		ver := &versions[idx]
		for _, oldver := range mreq.Versions {
			if ver.Version == oldver.Version {
				log.Infof("Copying patches %d for version %d", len(oldver.Patches), oldver.Version)
				ver.Patches = oldver.Patches
				ver.Metadata.Partial = false
			}
		}

		// We are only loading the most recent version by default
		// We'll add support for lazy loading older versions
		// once we add UI for choosing to display other versions
		if idx == (len(versions)-1) && ver.Metadata.Partial == true {
			patches, err := enginerepo.Source.GetPatches(mreq, ver)
			if err != nil {
				engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request patches: %s", err))
				continue
			}
			log.Infof("Fetching new patches %d", len(patches))

			ver.Patches = patches
			ver.Metadata.Partial = false
		}
	}

	mreq.Versions = versions

	threads, err := enginerepo.Source.GetMergeRequestThreads(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request threads: %s", err))
		return
	}

	mreq.Threads = threads

	approvals, err := enginerepo.Source.GetMergeRequestApprovals(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request approvals: %s", err))
		return
	}

	mreq.Approvals = approvals

	mreq.Metadata.Partial = false
}

func (engine *engineImpl) refreshMergeRequests(enginerepo *engineImplRepo) {
	log.Infof("Refresh merged requests %s", enginerepo.Repo.String())
	engine.Cache.LoadRepo(&enginerepo.Repo)
	// By default we want to query merge requests in all
	// states, so that we see transitions.
	//
	// The first time around, however, we're only going
	// to ask for open mreqs, as we don't need anything
	// that was closed before we got involved
	onlyOpen := false
	enginerepo.RefreshedAt = time.Now()
	if enginerepo.Repo.UpdatedAt == nil {
		log.Infof("No updatedAt time for repo %s", enginerepo.Repo.String())
		var newest *time.Time
		for _, mreq := range enginerepo.MergeRequests {
			if newest == nil {
				newest = &mreq.UpdatedAt
			} else {
				if mreq.UpdatedAt.After(*newest) {
					newest = &mreq.UpdatedAt
				}
			}
		}

		if newest == nil {
			onlyOpen = true
		} else {
			enginerepo.Repo.UpdatedAt = newest
			log.Infof("Newest mreq %s for repo %s", newest.String(), enginerepo.Repo.String())
		}
	}

	engine.Listener.Status(fmt.Sprintf("Querying current merge requests %s", enginerepo.Repo.String()))
	then := time.Now().UTC()
	mreqs, err := enginerepo.Source.GetMergeRequests(onlyOpen, enginerepo.Repo.UpdatedAt)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to query merge requests: %s", err))
		return
	}

	enginerepo.Repo.UpdatedAt = &then

	mreqsToUpdate := make([]model.MergeReq, 0)
	for _, mreq := range mreqs {
		cachedmreq, ok := enginerepo.MergeRequests[mreq.ID]

		if ok {
			// We never want to loose info, so we'll copy the cached
			// versions and refresh it later
			mreq.Versions = cachedmreq.Versions
			mreq.Metadata.Status = model.STATUS_UPDATED

			if cachedmreq.UpdatedAt.Before(mreq.UpdatedAt) || cachedmreq.Metadata.Partial {
				log.Infof("Mreq %s is outdated", mreq.String())
				mreqsToUpdate = append(mreqsToUpdate, mreq)
			} else {
				log.Infof("Mreq %s is up2date", mreq.String())
			}
		} else {
			log.Infof("Mreq %s is not cached", mreq.String())
			mreqsToUpdate = append(mreqsToUpdate, mreq)
		}
	}
	mreqs = mreqsToUpdate

	if len(mreqs) > 0 {
		engine.Listener.Status(fmt.Sprintf("Fetching %d merge requests", len(mreqs)))
	}
	for idx, _ := range mreqs {
		mreq := &mreqs[idx]
		engine.Listener.Status(fmt.Sprintf("Fetching merge request %d from source [%d/%d]",
			mreq.ID, idx+1, len(mreqs)))
		engine.refreshMergeRequest(enginerepo, mreq)

		engine.replaceMergeRequest(enginerepo, *mreq)
	}

	for idx, _ := range enginerepo.MergeRequests {
		dupemreq := *enginerepo.MergeRequests[idx]
		if dupemreq.Repo.NickName == enginerepo.Repo.NickName {
			continue
		}
		engine.Listener.Status(fmt.Sprintf("Repo rename on merge request %d [%d/%d]",
			dupemreq.ID, idx+1, len(enginerepo.MergeRequests)))
		dupemreq.Repo.NickName = enginerepo.Repo.NickName
		engine.replaceMergeRequest(enginerepo, dupemreq)
	}

	engine.Listener.Status("")
	engine.Cache.SaveRepo(&enginerepo.Repo)
}

func (engine *engineImpl) addRepo(repo model.Repo) (*engineImplRepo, error) {
	log.Infof("Adding repo %s", repo.String())

	sourceimpl, err := source.NewGitLabForRepo(engine.TLSConfig, engine.HTTPProxy, &repo)
	if err != nil {
		return nil, err
	}

	err = engine.Cache.AddRepo(&repo)
	if err != nil {
		return nil, err
	}

	repoimpl := engineImplRepo{
		Repo:          repo,
		MergeRequests: make(map[uint]*model.MergeReq),
		Source:        sourceimpl,
	}

	engine.Repos = append(engine.Repos, repoimpl)

	return &engine.Repos[len(engine.Repos)-1], nil
}

func (engine *engineImpl) updateRepo(repo model.Repo) (*engineImplRepo, error) {
	log.Infof("Update repo %s", repo.String())

	sourceimpl, err := source.NewGitLabForRepo(engine.TLSConfig, engine.HTTPProxy, &repo)
	if err != nil {
		return nil, err
	}

	for idx, _ := range engine.Repos {
		repoimpl := &engine.Repos[idx]

		if repo.Equal(&repoimpl.Repo) {
			repoimpl.Repo = repo
			repoimpl.Source = sourceimpl
			return repoimpl, nil
		}
	}

	return nil, fmt.Errorf("No current repo matching %s", repo.String())
}

func (engine *engineImpl) getEngineRepo(repo model.Repo) *engineImplRepo {
	for idx, _ := range engine.Repos {
		impl := &engine.Repos[idx]
		if impl.Repo.Equal(&repo) {
			return impl
		}
	}

	return nil
}

func (engine *engineImpl) updateLoop() {
	log.Infof("Update loop running")
	for idx, _ := range engine.Repos {
		repo := &engine.Repos[idx]
		log.Infof("Loading cache for repo %s", repo.Repo.String())
		engine.loadMergeRequestCache(repo)
	}

	for {
		log.Info("Selecting job")
		if len(engine.RefreshTimer.C) != 0 {
			<-engine.RefreshTimer.C
			go engine.RefreshRepos()
		} else if len(engine.PriorityJobs) != 0 {
			job := <-engine.PriorityJobs
			job.Run(engine)
		} else if len(engine.BackgroundJobs) != 0 {
			job := <-engine.BackgroundJobs
			job.Run(engine)
		} else {
			select {
			case <-engine.RefreshTimer.C:
				go engine.RefreshRepos()

			case job := <-engine.PriorityJobs:
				job.Run(engine)

			case job := <-engine.BackgroundJobs:
				job.Run(engine)
			}
		}

		log.Info("Next loop iteration")
	}
}

func (engine *engineImpl) AddRepository(repo model.Repo) {
	log.Infof("Queue add repo %s", repo.String())
	engine.PriorityJobs <- &engineAddRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) UpdateRepository(repo model.Repo) {
	log.Infof("Queue update repo %s", repo.String())
	engine.PriorityJobs <- &engineUpdateRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) RemoveRepository(repo model.Repo) {
	log.Infof("Queue remove repo %s", repo.String())
	engine.PriorityJobs <- &engineRemoveRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) RefreshRepos() {
	log.Info("Queue refresh repos")
	engine.PriorityJobs <- &engineRefreshReposJob{
		QueuedAt: time.Now(),
	}
}

func (engine *engineImpl) RefreshRepositoryAt(repo model.Repo, when time.Time) {
	log.Infof("Queue refresh repo %s", repo.String())
	engine.BackgroundJobs <- &engineRefreshRepoJob{
		Repo:     repo,
		QueuedAt: when,
	}
}

func (engine *engineImpl) RefreshRepository(repo model.Repo) {
	engine.RefreshRepositoryAt(repo, time.Now())
}

func (engine *engineImpl) RefreshMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue refresh merge request %s", mreq.String())
	engine.PriorityJobs <- &engineRefreshMergeRequestJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) LoadMergeRequestCommitDiffs(mreq model.MergeReq, commit model.Commit) {
	log.Infof("Queue load merge request commit diffs %s diff %s",
		mreq.String(), commit.Hash)
	engine.PriorityJobs <- &engineLoadMergeRequestCommitDiffsJob{
		MergeReq: mreq,
		Commit:   commit,
	}
}

func (engine *engineImpl) LoadMergeRequestSeriesPatches(mreq model.MergeReq, series model.Series) {
	log.Infof("Queue load merge request series patches %s series %d %d",
		mreq.String(), series.Index, series.Version)
	engine.PriorityJobs <- &engineLoadMergeRequestSeriesPatchesJob{
		MergeReq: mreq,
		Series:   series,
	}
}

func (engine *engineImpl) AddMergeRequestThread(mreq model.MergeReq, text string, standalone bool, context *model.CommentContext) {
	log.Infof("Queue add merge request thread %s", mreq.String())
	engine.PriorityJobs <- &engineAddMergeRequestThreadJob{
		MergeReq:   mreq,
		Text:       text,
		Standalone: standalone,
		Context:    context,
	}
}

func (engine *engineImpl) AddMergeRequestReply(mreq model.MergeReq, thread, text string) {
	log.Infof("Queue add merge request reply %s", mreq.String())
	engine.PriorityJobs <- &engineAddMergeRequestReplyJob{
		MergeReq: mreq,
		Thread:   thread,
		Text:     text,
	}
}

func (engine *engineImpl) ResolveMergeRequestThread(mreq model.MergeReq, thread string, resolved bool) {
	log.Infof("Queue resolve merge request thread job %s", mreq.String())
	engine.PriorityJobs <- &engineResolveMergeRequestThreadJob{
		MergeReq: mreq,
		Thread:   thread,
		Resolved: resolved,
	}
}

func (engine *engineImpl) MarkRead(mreq model.MergeReq) {
	log.Infof("Queue mark read job %s", mreq.String())
	engine.PriorityJobs <- &engineMergeRequestMarkReadJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) AcceptMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue merge request accept %s", mreq.String())
	engine.PriorityJobs <- &engineMergeRequestAcceptJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) ApproveMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue merge request approve %s", mreq.String())
	engine.PriorityJobs <- &engineMergeRequestApproveJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) UnapproveMergeRequest(mreq model.MergeReq) {
	log.Infof("Queue merge request unapprove %s", mreq.String())
	engine.PriorityJobs <- &engineMergeRequestUnapproveJob{
		MergeReq: mreq,
	}
}
