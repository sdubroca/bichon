// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type OverviewPageListener interface {
	OverviewPageQuit()
	OverviewPageRefreshMergeRequest(mreq *model.MergeReq)
	OverviewPageAcceptMergeReq(mreq *model.MergeReq)
	OverviewPageApproveMergeReq(mreq *model.MergeReq)
	OverviewPageUnapproveMergeReq(mreq *model.MergeReq)
	OverviewPagePickVersion()
	OverviewPageChangePatch(num int)
}

type OverviewPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    OverviewPageListener
	Patches     *tview.Table

	MergeReq *model.MergeReq

	Version int
}

func NewOverviewPage(app *tview.Application, listener OverviewPageListener) *OverviewPage {
	patches := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.StyleDefault.
			Foreground(GetStyleColor(ELEMENT_MREQS_ACTIVE_TEXT)).
			Background(GetStyleColor(ELEMENT_MREQS_ACTIVE_FILL)).
			Attributes(GetStyleAttrMask(ELEMENT_MREQS_ACTIVE_ATTR))).
		SetFixed(1, 0)

	layout := tview.NewFrame(patches).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &OverviewPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		Patches:     patches,
	}

	return page
}

func (page *OverviewPage) GetName() string {
	return "overview"
}

func (page *OverviewPage) GetKeyShortcuts() string {
	return "q:Index r:Refresh a:Approve A:Unapprove m:Merge v:Version"
}

func (page *OverviewPage) buildMergeReqRow(mreq *model.MergeReq, version int, series *model.Series) [7]*tview.TableCell {
	npatches := len(series.Patches)

	return [7]*tview.TableCell{
		&tview.TableCell{
			Text:            fmt.Sprintf("%5s", fmt.Sprintf("#%d", mreq.ID)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Repo.NickName, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", mreq.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", tview.Escape(TrimEllipsisFront(mreq.Submitter.RealName, 20))),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%4s", fmt.Sprintf("v%d", version)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%7s", fmt.Sprintf("0/%d", npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(mreq.Title),
			Expansion:       1,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *OverviewPage) buildMergeReqPatchRow(mreq *model.MergeReq, series *model.Series, idx int) [7]*tview.TableCell {
	patch := &series.Patches[idx]
	npatches := len(series.Patches)

	var marker string
	if idx == (len(series.Patches) - 1) {
		marker = "└─>  "
	} else {
		marker = "├─>  "
	}
	return [7]*tview.TableCell{
		&tview.TableCell{
			Text:            "",
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            marker,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", patch.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", tview.Escape(TrimEllipsisFront(patch.Author.Name, 20))),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            "",
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%7s", fmt.Sprintf("%d/%d", idx+1, npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(patch.Title),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *OverviewPage) refreshMain(app *tview.Application, mreq *model.MergeReq) {
	log.Infof("Refreshing detail page main")
	if page.MergeReq == nil || !page.MergeReq.Equal(mreq) {
		log.Infof("Reset content line")
		page.Version = 0
		page.Patches.Select(0, 0)
	}
	page.MergeReq = mreq
	page.updatePatches()
}

func (page *OverviewPage) Refresh(app *tview.Application, mreq *model.MergeReq) {
	log.Infof("Refreshing detail page queue")
	go app.QueueUpdateDraw(func() {
		page.refreshMain(app, mreq)
	})
}

func (page *OverviewPage) SelectPatch(num int) {
	row, col := page.Patches.GetSelection()
	if row == num {
		return
	}
	if num < page.Patches.GetRowCount() {
		page.Patches.Select(num, col)
	}
}

func (page *OverviewPage) SwitchVersion(version int) {
	log.Infof("Switch to version %d", version)
	if version > len(page.MergeReq.Versions) {
		return
	}
	page.Version = version
	page.updatePatches()
}

func (page *OverviewPage) Activate() {
	page.Application.SetFocus(page.Patches)
}

func (page *OverviewPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'q':
			page.Listener.OverviewPageQuit()
		case 'm':
			if page.MergeReq != nil {
				page.Listener.OverviewPageAcceptMergeReq(page.MergeReq)
			}
		case 'a':
			if page.MergeReq != nil {
				page.Listener.OverviewPageApproveMergeReq(page.MergeReq)
			}
		case 'A':
			if page.MergeReq != nil {
				page.Listener.OverviewPageUnapproveMergeReq(page.MergeReq)
			}
		case 'r':
			if page.MergeReq != nil {
				page.Listener.OverviewPageRefreshMergeRequest(page.MergeReq)
			}
		case 'v':
			if page.MergeReq != nil {
				page.Listener.OverviewPagePickVersion()
			}
		case ' ':
			row, _ := page.Patches.GetSelection()
			page.Listener.OverviewPageChangePatch(row)
		default:
			return event
		}

	case tcell.KeyEnter:
		row, _ := page.Patches.GetSelection()
		page.Listener.OverviewPageChangePatch(row)

	case tcell.KeyLeft:
		row, col := page.Patches.GetSelection()
		if row < 0 {
			break
		}
		if row > 0 {
			page.Patches.Select(row-1, col)
		}
	case tcell.KeyRight:
		row, col := page.Patches.GetSelection()
		if row < 0 {
			break
		}
		if row < (page.Patches.GetRowCount() - 1) {
			page.Patches.Select(row+1, col)
		}
	case tcell.KeyEscape:
		page.Listener.OverviewPageQuit()
	default:
		return event
	}

	return nil
}

func (page *OverviewPage) updatePatches() {
	page.Patches.Clear()

	mreq := page.MergeReq
	if mreq == nil {
		return
	}

	cells := make([][7]*tview.TableCell, 0)

	var version int
	var series *model.Series
	if page.Version == 0 {
		version = len(mreq.Versions)
	} else {
		version = page.Version
	}
	if version != 0 {
		series = &mreq.Versions[version-1]
	}
	cells = append(cells, page.buildMergeReqRow(mreq, version, series))
	for idx, _ := range series.Patches {
		cells = append(cells, page.buildMergeReqPatchRow(mreq, series, idx))
	}

	for idx, row := range cells {
		page.Patches.SetCell(idx, 0, row[0])
		page.Patches.SetCell(idx, 1, row[1])
		page.Patches.SetCell(idx, 2, row[2])
		page.Patches.SetCell(idx, 3, row[3])
		page.Patches.SetCell(idx, 4, row[4])
		page.Patches.SetCell(idx, 5, row[5])
		page.Patches.SetCell(idx, 6, row[6])
	}
	page.Patches.SetOffset(0, 0)
	row, col := page.Patches.GetSelection()
	if row >= page.Patches.GetRowCount() {
		row = page.Patches.GetRowCount() - 1
		page.Patches.Select(row, col)
	}
}
