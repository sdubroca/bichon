// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell/v2"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type EditProjectPageListener interface {
	EditProjectPageCancel()
	EditProjectPageConfirm(repo model.Repo)
}

type EditProjectPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    EditProjectPageListener
	Details     *tview.Form
	Directory   *tview.InputField
	Remote      *tview.InputField
	NickName    *tview.InputField
	Server      *tview.InputField
	Project     *tview.InputField
	Token       *tview.InputField
	Global      *tview.Checkbox
	State       *tview.DropDown

	Repo model.Repo
}

func NewEditProjectPage(app *tview.Application, listener EditProjectPageListener) *EditProjectPage {
	details := tview.NewForm()

	directory := addInputField(details, "Directory", "", 40, true)
	remote := addInputField(details, "Remote", "origin", 20, true)
	nickname := addInputField(details, "Nick name", "", 40, false)
	server := addInputField(details, "Server", "", 20, true)
	project := addInputField(details, "Project", "", 20, true)
	token := addInputField(details, "API Token", "", 40, false)
	mask := addCheckbox(details, "", "Show API token", false, false)
	global := addCheckbox(details, "Global", "Use token for all repos on this server", true, false)
	state := addDropDown(details, "State", []string{
		"Active",
		"Inactive",
		"Hidden",
	}, false)

	// XXX use a unicode mask but InputField is broken
	// wrt measuring character size
	token.SetMaskCharacter('*')

	mask.SetChangedFunc(func(checked bool) {
		if checked {
			token.SetMaskCharacter(rune(0))
		} else {
			token.SetMaskCharacter('*')
		}
	})

	details.SetBorder(true).
		SetTitle("Edit existing project")

	layout := tview.NewFrame(details).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &EditProjectPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		Details:     details,
		Directory:   directory,
		Remote:      remote,
		NickName:    nickname,
		Server:      server,
		Project:     project,
		Token:       token,
		Global:      global,
		State:       state,
	}

	details.AddDefaultButton("Save", page.confirmButton)
	details.AddButton("Cancel", page.cancelButton)
	details.SetCancelFunc(page.cancelButton)

	return page
}

func (page *EditProjectPage) GetName() string {
	return "editproject"
}

func (page *EditProjectPage) GetKeyShortcuts() string {
	return "F10:Confirm Esc:Cancel"
}

func (page *EditProjectPage) SetRepo(repo model.Repo) {
	page.Repo = repo
	page.Directory.SetText(page.Repo.Directory)
	page.Remote.SetText(page.Repo.Remote)
	page.NickName.SetText(page.Repo.NickName)
	page.Server.SetText(page.Repo.Server)
	page.Project.SetText(page.Repo.Project)
	page.Token.SetText(page.Repo.Token)
	page.Global.SetChecked(page.Repo.GlobalToken)
	if page.Repo.State == model.RepoStateActive {
		page.State.SetCurrentOption(0)
	} else if page.Repo.State == model.RepoStateInactive {
		page.State.SetCurrentOption(1)
	} else if page.Repo.State == model.RepoStateHidden {
		page.State.SetCurrentOption(2)
	} else {
		page.State.SetCurrentOption(0)
	}
}

func (page *EditProjectPage) Activate() {
	page.Details.SetFocus(0)
	page.Application.SetFocus(page.Details)
}

func (page *EditProjectPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	return event
}

func (page *EditProjectPage) clearText() {
	page.Directory.SetText("")
	page.Remote.SetText("origin")
	page.NickName.SetText("")
	page.Server.SetText("")
	page.Project.SetText("")
	page.Token.SetText("")
	page.Global.SetChecked(true)
	page.State.SetCurrentOption(0)
}

func (page *EditProjectPage) confirmButton() {
	page.Repo.Token = page.Token.GetText()
	page.Repo.GlobalToken = page.Global.IsChecked()

	state, _ := page.State.GetCurrentOption()
	if state == -1 {
		page.Repo.State = model.RepoStateActive
	} else {
		states := []model.RepoState{
			model.RepoStateActive,
			model.RepoStateInactive,
			model.RepoStateHidden,
		}
		page.Repo.State = states[state]
	}
	nickname := page.NickName.GetText()
	if nickname == "" {
		nickname = model.GenerateRepoNickName(page.Repo.Directory, page.Repo.Remote)
	}
	page.Repo.NickName = nickname

	page.clearText()
	page.Listener.EditProjectPageConfirm(page.Repo)
}

func (page *EditProjectPage) cancelButton() {
	page.clearText()
	page.Listener.EditProjectPageCancel()
}
