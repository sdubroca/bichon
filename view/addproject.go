// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type AddProjectPageListener interface {
	AddProjectPageCancel()
	AddProjectPageConfirm(repo model.Repo)
	AddProjectPageAutoFillToken(server, project string) string
}

type AddProjectPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    AddProjectPageListener
	Details     *tview.Form
	Directory   *tview.InputField
	Remote      *tview.InputField
	NickName    *tview.InputField
	Server      *tview.InputField
	Project     *tview.InputField
	Token       *tview.InputField
	Global      *tview.Checkbox
}

func NewAddProjectPage(app *tview.Application, listener AddProjectPageListener) *AddProjectPage {
	details := tview.NewForm()

	directory := addInputField(details, "Directory", "", 40, false)
	remote := addInputField(details, "Remote", "origin", 20, false)
	nickname := addInputField(details, "Nick name", "", 40, false)
	server := addInputField(details, "Server", "", 20, false)
	project := addInputField(details, "Project", "", 20, false)
	token := addInputField(details, "API Token", "", 40, false)
	mask := addCheckbox(details, "", "Show API token", false, false)
	global := addCheckbox(details, "Global", "Use token for all repos on this server", true, false)

	// XXX use a unicode mask but InputField is broken
	// wrt measuring character size
	token.SetMaskCharacter('*')

	mask.SetChangedFunc(func(checked bool) {
		if checked {
			token.SetMaskCharacter(rune(0))
		} else {
			token.SetMaskCharacter('*')
		}
	})

	details.SetBorder(true).
		SetTitle("Add a new project")

	layout := tview.NewFrame(details).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &AddProjectPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		Details:     details,
		Directory:   directory,
		Remote:      remote,
		NickName:    nickname,
		Server:      server,
		Project:     project,
		Token:       token,
		Global:      global,
	}

	details.AddDefaultButton("Save", page.confirmButton)
	details.AddButton("Cancel", page.cancelButton)
	details.SetCancelFunc(page.cancelButton)

	page.Directory.SetChangedFunc(page.autoCompleteRepo)
	page.Remote.SetChangedFunc(page.autoCompleteRepo)

	project.SetChangedFunc(page.autoCompleteToken)

	return page
}

func (page *AddProjectPage) autoCompleteRepo(text string) {
	dir := page.Directory.GetText()
	remote := page.Remote.GetText()

	log.Infof("Auto complete repo for '%s' & '%s'", dir, remote)

	if dir == "" || remote == "" {
		return
	}

	repo, err := model.NewRepoForDirectory(dir, remote)
	if err != nil {
		return
	}

	page.NickName.SetText(repo.NickName)
	page.Server.SetText(repo.Server)
	page.Project.SetText(repo.Project)
}

func (page *AddProjectPage) autoCompleteToken(text string) {
	server := page.Server.GetText()
	project := page.Project.GetText()

	log.Infof("Auto complete token for '%s' & '%s'", server, project)

	token := page.Listener.AddProjectPageAutoFillToken(server, project)
	page.Token.SetText(token)
}

func (page *AddProjectPage) GetName() string {
	return "addproject"
}

func (page *AddProjectPage) GetKeyShortcuts() string {
	return "F10:Confirm Esc:Cancel"
}

func (page *AddProjectPage) Activate() {
	page.Details.SetFocus(0)
	page.Application.SetFocus(page.Details)
}

func (page *AddProjectPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	return event
}

func (page *AddProjectPage) clearText() {
	page.Directory.SetText("")
	page.Remote.SetText("origin")
	page.NickName.SetText("")
	page.Server.SetText("")
	page.Project.SetText("")
	page.Token.SetText("")
	page.Global.SetChecked(true)
}

func (page *AddProjectPage) confirmButton() {
	dir := page.Directory.GetText()
	remote := page.Remote.GetText()
	nickname := page.NickName.GetText()
	server := page.Server.GetText()
	proj := page.Project.GetText()
	token := page.Token.GetText()
	global := page.Global.IsChecked()

	if len(proj) > 0 && proj[0] == '/' {
		proj = proj[1:]
	}

	repo := model.NewRepo(nickname, dir, remote, server, proj, token, global, model.RepoStateActive)

	page.clearText()
	page.Listener.AddProjectPageConfirm(*repo)
}

func (page *AddProjectPage) cancelButton() {
	page.clearText()
	page.Listener.AddProjectPageCancel()
}

func (page *AddProjectPage) LoadLocalProject() {
	gitdir, err := model.FindGitDir()

	if err == nil {
		page.Directory.SetText(gitdir)
	}
}
