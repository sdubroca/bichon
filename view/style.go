// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/go-ini/ini"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config/xdg"
)

type StyleElement int

const (
	ELEMENT_PRIMITIVE_FILL StyleElement = iota
	ELEMENT_CONTRAST_FILL
	ELEMENT_MORE_CONTRAST_FILL

	ELEMENT_PRIMARY_TEXT
	ELEMENT_SECONDARY_TEXT
	ELEMENT_TERTIARY_TEXT
	ELEMENT_INVERSE_TEXT
	ELEMENT_CONTRAST_SECONDARY_TEXT

	ELEMENT_BORDER
	ELEMENT_TITLE
	ELEMENT_GRAPHICS

	ELEMENT_STATUS_TEXT
	ELEMENT_STATUS_FILL
	ELEMENT_STATUS_ATTR

	ELEMENT_SHORTCUTS_TEXT
	ELEMENT_SHORTCUTS_FILL
	ELEMENT_SHORTCUTS_ATTR

	ELEMENT_MREQS_ACTIVE_TEXT
	ELEMENT_MREQS_ACTIVE_FILL
	ELEMENT_MREQS_ACTIVE_ATTR

	ELEMENT_MREQS_INACTIVE_TEXT
	ELEMENT_MREQS_INACTIVE_FILL
	ELEMENT_MREQS_INACTIVE_ATTR

	ELEMENT_HEADER_TEXT
	ELEMENT_HEADER_FILL
	ELEMENT_HEADER_ATTR

	ELEMENT_SUMMARY_SEPARATOR_TEXT
	ELEMENT_SUMMARY_SEPARATOR_FILL
	ELEMENT_SUMMARY_SEPARATOR_ATTR

	ELEMENT_SUMMARY_HEADER_TEXT
	ELEMENT_SUMMARY_HEADER_FILL
	ELEMENT_SUMMARY_HEADER_ATTR

	ELEMENT_SUMMARY_TITLE_TEXT
	ELEMENT_SUMMARY_TITLE_FILL
	ELEMENT_SUMMARY_TITLE_ATTR

	ELEMENT_SUMMARY_DATE_TEXT
	ELEMENT_SUMMARY_DATE_FILL
	ELEMENT_SUMMARY_DATE_ATTR

	ELEMENT_SUMMARY_AUTHOR_TEXT
	ELEMENT_SUMMARY_AUTHOR_FILL
	ELEMENT_SUMMARY_AUTHOR_ATTR

	ELEMENT_COMMENT_QUOTED_TEXT
	ELEMENT_COMMENT_QUOTED_FILL
	ELEMENT_COMMENT_QUOTED_ATTR

	ELEMENT_PROJECTS_ACTIVE_TEXT
	ELEMENT_PROJECTS_ACTIVE_FILL
	ELEMENT_PROJECTS_ACTIVE_ATTR

	ELEMENT_PROJECTS_INACTIVE_TEXT
	ELEMENT_PROJECTS_INACTIVE_FILL
	ELEMENT_PROJECTS_INACTIVE_ATTR

	ELEMENT_REPORTS_ACTIVE_TEXT
	ELEMENT_REPORTS_ACTIVE_FILL
	ELEMENT_REPORTS_ACTIVE_ATTR

	ELEMENT_REPORTS_INACTIVE_TEXT
	ELEMENT_REPORTS_INACTIVE_FILL
	ELEMENT_REPORTS_INACTIVE_ATTR

	ELEMENT_DIFF_PREAMBLE_TEXT
	ELEMENT_DIFF_PREAMBLE_FILL
	ELEMENT_DIFF_PREAMBLE_ATTR

	ELEMENT_DIFF_CONTEXT_TEXT
	ELEMENT_DIFF_CONTEXT_FILL
	ELEMENT_DIFF_CONTEXT_ATTR

	ELEMENT_DIFF_ADDED_TEXT
	ELEMENT_DIFF_ADDED_FILL
	ELEMENT_DIFF_ADDED_ATTR

	ELEMENT_DIFF_REMOVED_TEXT
	ELEMENT_DIFF_REMOVED_FILL
	ELEMENT_DIFF_REMOVED_ATTR
)

var colorMap = map[StyleElement]string{
	ELEMENT_PRIMITIVE_FILL:     "black",
	ELEMENT_CONTRAST_FILL:      "blue",
	ELEMENT_MORE_CONTRAST_FILL: "green",

	ELEMENT_PRIMARY_TEXT:            "white",
	ELEMENT_SECONDARY_TEXT:          "yellow",
	ELEMENT_TERTIARY_TEXT:           "green",
	ELEMENT_INVERSE_TEXT:            "blue",
	ELEMENT_CONTRAST_SECONDARY_TEXT: "darkcyan",

	ELEMENT_BORDER:   "white",
	ELEMENT_TITLE:    "white",
	ELEMENT_GRAPHICS: "white",

	ELEMENT_STATUS_TEXT: "yellow",
	ELEMENT_STATUS_FILL: "blue",

	ELEMENT_SHORTCUTS_TEXT: "yellow",
	ELEMENT_SHORTCUTS_FILL: "blue",

	ELEMENT_MREQS_ACTIVE_TEXT: "yellow",
	ELEMENT_MREQS_ACTIVE_FILL: "red",

	ELEMENT_MREQS_INACTIVE_TEXT: "white",
	ELEMENT_MREQS_INACTIVE_FILL: "black",

	ELEMENT_HEADER_TEXT: "yellow",
	ELEMENT_HEADER_FILL: "blue",

	ELEMENT_SUMMARY_SEPARATOR_TEXT: "grey",
	ELEMENT_SUMMARY_SEPARATOR_FILL: "black",

	ELEMENT_SUMMARY_HEADER_TEXT: "white",
	ELEMENT_SUMMARY_HEADER_FILL: "black",

	ELEMENT_SUMMARY_TITLE_TEXT: "red",
	ELEMENT_SUMMARY_TITLE_FILL: "black",

	ELEMENT_SUMMARY_DATE_TEXT: "white",
	ELEMENT_SUMMARY_DATE_FILL: "black",

	ELEMENT_SUMMARY_AUTHOR_TEXT: "red",
	ELEMENT_SUMMARY_AUTHOR_FILL: "black",

	ELEMENT_COMMENT_QUOTED_TEXT: "purple",
	ELEMENT_COMMENT_QUOTED_FILL: "black",

	ELEMENT_PROJECTS_ACTIVE_TEXT: "yellow",
	ELEMENT_PROJECTS_ACTIVE_FILL: "red",

	ELEMENT_PROJECTS_INACTIVE_TEXT: "white",
	ELEMENT_PROJECTS_INACTIVE_FILL: "black",

	ELEMENT_REPORTS_ACTIVE_TEXT: "yellow",
	ELEMENT_REPORTS_ACTIVE_FILL: "red",

	ELEMENT_REPORTS_INACTIVE_TEXT: "white",
	ELEMENT_REPORTS_INACTIVE_FILL: "black",

	ELEMENT_DIFF_PREAMBLE_TEXT: "blue",
	ELEMENT_DIFF_PREAMBLE_FILL: "black",

	ELEMENT_DIFF_CONTEXT_TEXT: "yellow",
	ELEMENT_DIFF_CONTEXT_FILL: "black",

	ELEMENT_DIFF_ADDED_TEXT: "green",
	ELEMENT_DIFF_ADDED_FILL: "black",

	ELEMENT_DIFF_REMOVED_TEXT: "purple",
	ELEMENT_DIFF_REMOVED_FILL: "black",
}

var attrMap = map[StyleElement]string{
	ELEMENT_STATUS_ATTR:            "",
	ELEMENT_SHORTCUTS_ATTR:         "",
	ELEMENT_MREQS_ACTIVE_ATTR:      "bold",
	ELEMENT_MREQS_INACTIVE_ATTR:    "",
	ELEMENT_PROJECTS_ACTIVE_ATTR:   "bold",
	ELEMENT_PROJECTS_INACTIVE_ATTR: "",
	ELEMENT_REPORTS_ACTIVE_ATTR:    "bold",
	ELEMENT_REPORTS_INACTIVE_ATTR:  "",
	ELEMENT_HEADER_ATTR:            "",
	ELEMENT_SUMMARY_SEPARATOR_ATTR: "",
	ELEMENT_SUMMARY_HEADER_ATTR:    "bold",
	ELEMENT_SUMMARY_TITLE_ATTR:     "bold",
	ELEMENT_SUMMARY_DATE_ATTR:      "bold",
	ELEMENT_SUMMARY_AUTHOR_ATTR:    "bold",
	ELEMENT_COMMENT_QUOTED_ATTR:    "",
	ELEMENT_DIFF_PREAMBLE_ATTR:     "",
	ELEMENT_DIFF_CONTEXT_ATTR:      "",
	ELEMENT_DIFF_ADDED_ATTR:        "",
	ELEMENT_DIFF_REMOVED_ATTR:      "",
}

const sectionHelpColor = "" +
	"Most keys in this section expect a name from the W3C HTML colors list:\n" +
	" \n" +
	"    https://htmlcolorcodes.com/color-names/\n" +
	" \n" +
	"Color names should be given in all lowercase.\n" +
	" \n" +
	"Keys ending in '-attr' expect a comma separated list of the modifiers:\n" +
	" \n" +
	" 'bold', 'blink', 'dim', 'reverse', 'underline'"

var styleFieldNames = map[StyleElement]string{
	ELEMENT_PRIMITIVE_FILL:     "primitive-fill",
	ELEMENT_CONTRAST_FILL:      "contrast-fill",
	ELEMENT_MORE_CONTRAST_FILL: "more-contrast-fill",

	ELEMENT_PRIMARY_TEXT:            "primary-text",
	ELEMENT_SECONDARY_TEXT:          "secondary-text",
	ELEMENT_TERTIARY_TEXT:           "tertiary-text",
	ELEMENT_INVERSE_TEXT:            "inverse-text",
	ELEMENT_CONTRAST_SECONDARY_TEXT: "contrast-secondary-text",

	ELEMENT_BORDER:   "border",
	ELEMENT_TITLE:    "title",
	ELEMENT_GRAPHICS: "graphics",

	ELEMENT_STATUS_TEXT: "status-text",
	ELEMENT_STATUS_FILL: "status-fill",
	ELEMENT_STATUS_ATTR: "status-attr",

	ELEMENT_SHORTCUTS_TEXT: "shortcuts-text",
	ELEMENT_SHORTCUTS_FILL: "shortcuts-fill",
	ELEMENT_SHORTCUTS_ATTR: "shortcuts-attr",

	ELEMENT_MREQS_ACTIVE_TEXT: "mreqs-active-text",
	ELEMENT_MREQS_ACTIVE_FILL: "mreqs-active-fill",
	ELEMENT_MREQS_ACTIVE_ATTR: "mreqs-active-attr",

	ELEMENT_MREQS_INACTIVE_TEXT: "mreqs-inactive-text",
	ELEMENT_MREQS_INACTIVE_FILL: "mreqs-inactive-fill",
	ELEMENT_MREQS_INACTIVE_ATTR: "mreqs-inactive-attr",

	ELEMENT_HEADER_TEXT: "header-text",
	ELEMENT_HEADER_FILL: "header-fill",
	ELEMENT_HEADER_ATTR: "header-attr",

	ELEMENT_SUMMARY_SEPARATOR_TEXT: "summary-separator-text",
	ELEMENT_SUMMARY_SEPARATOR_FILL: "summary-separator-fill",
	ELEMENT_SUMMARY_SEPARATOR_ATTR: "summary-separator-attr",

	ELEMENT_SUMMARY_HEADER_TEXT: "summary-header-text",
	ELEMENT_SUMMARY_HEADER_FILL: "summary-header-fill",
	ELEMENT_SUMMARY_HEADER_ATTR: "summary-header-attr",

	ELEMENT_SUMMARY_TITLE_TEXT: "summary-title-text",
	ELEMENT_SUMMARY_TITLE_FILL: "summary-title-fill",
	ELEMENT_SUMMARY_TITLE_ATTR: "summary-title-attr",

	ELEMENT_SUMMARY_DATE_TEXT: "summary-date-text",
	ELEMENT_SUMMARY_DATE_FILL: "summary-date-fill",
	ELEMENT_SUMMARY_DATE_ATTR: "summary-date-attr",

	ELEMENT_SUMMARY_AUTHOR_TEXT: "summary-author-text",
	ELEMENT_SUMMARY_AUTHOR_FILL: "summary-author-fill",
	ELEMENT_SUMMARY_AUTHOR_ATTR: "summary-author-attr",

	ELEMENT_COMMENT_QUOTED_TEXT: "comment-quoted-text",
	ELEMENT_COMMENT_QUOTED_FILL: "comment-quoted-fill",
	ELEMENT_COMMENT_QUOTED_ATTR: "comment-quoted-attr",

	ELEMENT_PROJECTS_ACTIVE_TEXT: "projects-active-text",
	ELEMENT_PROJECTS_ACTIVE_FILL: "projects-active-fill",
	ELEMENT_PROJECTS_ACTIVE_ATTR: "projects-active-attr",

	ELEMENT_PROJECTS_INACTIVE_TEXT: "projects-inactive-text",
	ELEMENT_PROJECTS_INACTIVE_FILL: "projects-inactive-fill",
	ELEMENT_PROJECTS_INACTIVE_ATTR: "projects-inactive-attr",

	ELEMENT_REPORTS_ACTIVE_TEXT: "reports-active-text",
	ELEMENT_REPORTS_ACTIVE_FILL: "reports-active-fill",
	ELEMENT_REPORTS_ACTIVE_ATTR: "reports-active-attr",

	ELEMENT_REPORTS_INACTIVE_TEXT: "reports-inactive-text",
	ELEMENT_REPORTS_INACTIVE_FILL: "reports-inactive-fill",
	ELEMENT_REPORTS_INACTIVE_ATTR: "reports-inactive-attr",

	ELEMENT_DIFF_PREAMBLE_TEXT: "diff-preamble-text",
	ELEMENT_DIFF_PREAMBLE_FILL: "diff-preamble-fill",
	ELEMENT_DIFF_PREAMBLE_ATTR: "diff-preamble-attr",

	ELEMENT_DIFF_CONTEXT_TEXT: "diff-context-text",
	ELEMENT_DIFF_CONTEXT_FILL: "diff-context-fill",
	ELEMENT_DIFF_CONTEXT_ATTR: "diff-context-attr",

	ELEMENT_DIFF_ADDED_TEXT: "diff-added-text",
	ELEMENT_DIFF_ADDED_FILL: "diff-added-fill",
	ELEMENT_DIFF_ADDED_ATTR: "diff-added-attr",

	ELEMENT_DIFF_REMOVED_TEXT: "diff-removed-text",
	ELEMENT_DIFF_REMOVED_FILL: "diff-removed-fill",
	ELEMENT_DIFF_REMOVED_ATTR: "diff-removed-attr",
}

type StyleOption int

const (
	OPTION_ALLOW_EMOJI StyleOption = iota
)

var optionMap = map[StyleOption]bool{
	OPTION_ALLOW_EMOJI: true,
}

var styleOptionNames = map[StyleOption]string{
	OPTION_ALLOW_EMOJI: "allow-emoji",
}

func LoadStyleConfig() error {
	stylepath := xdg.ConfigPath("style.ini")

	log.Infof("Loading style config from path %s", stylepath)
	cfg, err := ini.Load(stylepath)
	if err != nil {
		if os.IsNotExist(err) {
			SaveStyleConfig()
			cfg, err = ini.Load(stylepath)
			if err != nil {
				return err
			}
		}
		return err
	}

	colors := cfg.Section("colors")
	if colors != nil {
		for element, _ := range colorMap {
			key := styleFieldNames[element]
			val := colors.Key(key)

			if val != nil {
				color := val.String()
				if color != "" {
					log.Infof("Set UI element %s color to %s", key, color)
					colorMap[element] = color
				}
			}
		}

		for element, _ := range attrMap {
			key := styleFieldNames[element]
			var val *ini.Key

			if colors.HasKey(key) {
				val = colors.Key(key)
			}

			if val != nil {
				attr := val.String()
				log.Infof("Set UI element %s attr to %s", key, attr)
				attrMap[element] = attr
			}
		}
	}
	options := cfg.Section("options")
	if options != nil {
		for option, _ := range optionMap {
			key := styleOptionNames[option]
			val := options.Key(key)

			if val != nil {
				boolVal, err := val.Bool()
				if err == nil {
					log.Infof("Set UI option %s to %t", key, boolVal)
					optionMap[option] = boolVal
				}
			}
		}
	}

	return nil
}

func addToSection(section *ini.Section, settings map[string]string) {
	settingNames := make([]string, 0)
	for key, _ := range settings {
		settingNames = append(settingNames, key)
	}
	sort.Strings(settingNames)
	for _, key := range settingNames {
		section.NewKey(key, settings[key])
	}
}

func SaveStyleConfig() error {
	cfg := ini.Empty()
	colors, _ := cfg.NewSection("colors")
	colors.Comment = sectionHelpColor

	settings := make(map[string]string, 0)
	for element, value := range colorMap {
		key := styleFieldNames[element]
		settings[key] = value
	}

	for element, value := range attrMap {
		key := styleFieldNames[element]
		settings[key] = value
	}

	addToSection(colors, settings)

	options, _ := cfg.NewSection("options")
	settings = make(map[string]string, 0)
	for option, value := range optionMap {
		key := styleOptionNames[option]
		if value {
			settings[key] = "yes"
		} else {
			settings[key] = "no"
		}
	}
	addToSection(options, settings)

	stylepath := xdg.ConfigPath("style.ini")

	log.Infof("Saving style config to path %s", stylepath)

	cfgdir := filepath.Dir(stylepath)
	err := os.MkdirAll(cfgdir, 0700)
	if err != nil {
		return err
	}

	err = cfg.SaveTo(stylepath)
	if err != nil {
		return err
	}

	return nil
}

func GetStyleColorName(element StyleElement) string {
	return colorMap[element]
}

func GetStyleAttrName(element StyleElement) string {
	name := attrMap[element]

	attr := ""
	for _, c := range strings.Split(name, ",") {
		c = strings.TrimSpace(c)
		switch c {
		case "blink":
			attr += "l"

		case "bold":
			attr += "b"

		case "dim":
			attr += "d"

		case "reverse":
			attr += "r"

		case "underline":
			attr += "u"
		}
	}

	return attr
}

func GetStyleColor(element StyleElement) tcell.Color {
	name := colorMap[element]

	return tcell.GetColor(name)
}

func GetStyleAttrMask(element StyleElement) tcell.AttrMask {
	name := attrMap[element]

	var attr tcell.AttrMask

	for _, c := range strings.Split(name, ",") {
		c = strings.TrimSpace(c)
		log.Infof("Attr %s", c)
		switch c {
		case "blink":
			attr |= tcell.AttrBlink

		case "bold":
			attr |= tcell.AttrBold

		case "dim":
			attr |= tcell.AttrDim

		case "reverse":
			attr |= tcell.AttrReverse

		case "underline":
			attr |= tcell.AttrUnderline
		}
	}

	return attr
}

func GetStyleMarker(text, fill, attr StyleElement) string {
	return fmt.Sprintf("[%s:%s:%s]",
		GetStyleColorName(text),
		GetStyleColorName(fill),
		GetStyleAttrName(attr))
}

func GetStyleOption(option StyleOption) bool {
	return optionMap[option]
}

func GetStyleAltStr(option StyleOption, trueStr, falseStr string) string {
	if optionMap[option] {
		return trueStr
	} else {
		return falseStr
	}
}
