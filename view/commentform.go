// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package view

import (
	"fmt"

	"github.com/gdamore/tcell/v2"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type CommentFormListener interface {
	CommentFormSave(text string, standAlone bool)
	CommentFormCancel()
}

type CommentForm struct {
	*tview.Frame

	Flex        *tview.Flex
	Message     *tview.TextView
	Text        *tview.TextArea
	Form        *tview.Form
	Listener    CommentFormListener
	StartThread *tview.Checkbox

	Context *model.CommentContext
}

func NewCommentForm(listener CommentFormListener) *CommentForm {
	flex := tview.NewFlex()
	frame := tview.NewFrame(flex)
	form := &CommentForm{
		Frame: frame,

		Flex:     flex,
		Message:  tview.NewTextView(),
		Text:     tview.NewTextArea(),
		Form:     tview.NewForm(),
		Listener: listener,
	}

	form.SetBorder(true).SetTitle("Add comment")
	form.Text.SetBackgroundColor(GetStyleColor(ELEMENT_CONTRAST_FILL))

	form.StartThread = addCheckbox(form.Form, "", "Enable replies to comment", true, false)

	form.Form.AddDefaultButton("Save", form.buttonSave)
	form.Form.AddButton("Cancel", form.buttonCancel)

	flex.SetDirection(tview.FlexRow)
	flex.AddItem(form.Message, 1, 1, false)
	flex.AddItem(form.Text, 0, 1, false)
	flex.AddItem(form.Form, 4, 1, false)

	return form
}

func (form *CommentForm) buttonSave() {
	text := form.Text.GetText()
	standAlone := !form.StartThread.IsChecked()
	form.Listener.CommentFormSave(text, standAlone)
	form.Text.SetText("")
}

func (form *CommentForm) buttonCancel() {
	form.Listener.CommentFormCancel()
}

func (form *CommentForm) Activate(app *tview.Application, context *model.CommentContext, reply int, allowStandalone bool) {
	form.Text.SetText("")
	app.SetFocus(form.Text)

	form.Form.Clear(false)
	if allowStandalone {
		form.StartThread.SetChecked(false)
		form.Form.AddFormItem(form.StartThread)
		form.Flex.ResizeItem(form.Form, 5, 1)
	} else {
		form.StartThread.SetChecked(true)
		form.Flex.ResizeItem(form.Form, 3, 1)
	}

	if context != nil {
		if reply != -1 {
			form.SetTitle(fmt.Sprintf("Reply to commit diff comment #%d", reply+1))
		} else {
			form.SetTitle("Add commit diff comment")
		}
		var msg string
		if context.OldFile != "" && context.OldLine != 0 {
			msg += fmt.Sprintf("old file %s:%d", context.OldFile, context.OldLine)
		}
		if context.NewFile != "" && context.NewLine != 0 {
			if msg != "" {
				msg += ", "
			}
			msg += fmt.Sprintf("new file %s:%d", context.NewFile, context.NewLine)
		}
		form.Message.SetText("Commenting at " + msg)
	} else {
		if reply != -1 {
			form.SetTitle(fmt.Sprintf("Reply to merge request comment #%d", reply+1))
		} else {
			form.SetTitle("Add merge request comment")
		}
		form.Message.SetText("Commenting on cover letter")
	}
}

func (form *CommentForm) SetFocus(app *tview.Application, reverse bool) {
	if !reverse {
		app.SetFocus(form.Text)
	} else {
		form.Form.SetFocus(form.Form.GetButtonCount() - 1)
		app.SetFocus(form.Form)
	}
}

func (form *CommentForm) HandleFocusNav(app *tview.Application, event *tcell.EventKey, outside tview.Primitive) (*tcell.EventKey, bool) {
	if form.Text.HasFocus() {
		switch event.Key() {
		case tcell.KeyTab:
			form.Form.SetFocus(0)
			app.SetFocus(form.Form)
		case tcell.KeyBacktab:
			app.SetFocus(outside)
		default:
			return event, true
		}
		return nil, true
	}

	if form.Form.HasFocus() {
		if form.Form.GetButton(form.Form.GetButtonCount()-1).HasFocus() &&
			event.Key() == tcell.KeyTab {
			app.SetFocus(outside)
			return nil, true
		}
		if form.Form.GetButton(0).HasFocus() &&
			event.Key() == tcell.KeyBacktab {
			app.SetFocus(form.Text)
			return nil, true
		}
		return event, true

	}

	return event, false
}
