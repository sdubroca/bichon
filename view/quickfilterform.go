// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell/v2"

	"gitlab.com/bichon-project/bichon/model/filter"
	"gitlab.com/bichon-project/tview"
)

const helpMessage = ` Expression: Term ( ('&' | '|') Term)*
 Term: '!'? Match | '(' Expression ')'
 Match: Project | Real name | User name | Age |
        Activity | State | Status | Label | WIP |
        Approved real name | Approved user name

 Project name:        ~p STRING [MODIFIERS]
 Real name:          ~n STRING [MODIFIERS]
 User name:          ~u STRING [MODIFIERS]
 Age:                ~a TIME
 Activity:           ~t TIME
 State:              ~s o|c|m|l|open|closed|merged|locked
 Status:             ~m n|u|o|r|new|updated|old|read
 Label:              ~l STRING  (exact match)
 WIP:                ~w
 Approved            ~v COUNT (('<'|'>'|'='|'>='|'<=') VAL)
 Approved real name: ~vn STRING [MODIFIERS]
 Approved user name: ~vu STRING [MODIFIERS]

 MODIFIERS = /i (case fold) | /g (match glob)
 STRING = a-Z, 0-9, -, /, *, ?, [, ], ., : | "..." | '...'
 TIME = 0-9 year|month|mon|week|day|hour|minute|min|second|sec

 e.g.  ~p /bichon & ~su berrange & ~t 1 week & ! ~m read
`

type QuickFilterForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener FilterFormListener
	Error    *tview.TextView

	Filter       *tview.InputField
	FilterString string
}

func (form *QuickFilterForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.FilterFormCancel()
	form.Error.SetText("")
	form.Filter.SetText(form.FilterString)
}

func (form *QuickFilterForm) confirmFunc() {
	form.Form.SetFocus(0)
	filterString := form.Filter.GetText()
	info := filter.BuildFilter(filterString)
	if info.Error == nil {
		form.Error.SetText("")
		form.FilterString = info.Expression
		form.Listener.FilterFormConfirm(info)
	} else {
		form.Error.SetText("[red::]" + info.Error.Error() + "[::]")
	}
}

func NewQuickFilterForm(listener FilterFormListener) *QuickFilterForm {

	form := &QuickFilterForm{
		Form:     tview.NewForm(),
		Error:    tview.NewTextView(),
		Listener: listener,
	}

	form.Error.SetDynamicColors(true)

	layout := tview.NewFlex()
	help := tview.NewTextView()
	help.SetText(helpMessage)
	layout.SetBackgroundColor(tview.Styles.PrimitiveBackgroundColor)
	layout.SetBorder(true)
	layout.SetTitle("Merge request filtering")
	layout.SetDirection(tview.FlexRow)
	layout.AddItem(form.Form, 6, 0, true)
	layout.AddItem(form.Error, 3, 0, false)
	layout.AddItem(help, 0, 1, false)
	form.Primitive = Modal(layout, 68, 35)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Filter = addInputField(form.Form, "Filter", "", 60, false)
	form.Filter.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyEnter {
			form.confirmFunc()
			return nil
		}
		return event
	})
	form.Form.AddDefaultButton("Apply", form.confirmFunc)
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *QuickFilterForm) SetMergeReqFilter(filterexpr string) {
	form.Filter.SetText(filterexpr)
}
