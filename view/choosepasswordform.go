// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/tview"
)

type ChoosePasswordFormListener interface {
	ChoosePasswordFormConfirm(password string)
}
type ChoosePasswordForm struct {
	tview.Primitive

	Layout   *tview.Flex
	Info     *tview.TextView
	Form     *tview.Form
	Warning  *tview.TextView
	Listener ChoosePasswordFormListener

	Password      string
	PasswordAgain string
}

func NewChoosePasswordForm(listener ChoosePasswordFormListener) *ChoosePasswordForm {

	form := &ChoosePasswordForm{
		Form:     tview.NewForm(),
		Info:     tview.NewTextView(),
		Layout:   tview.NewFlex(),
		Warning:  tview.NewTextView(),
		Listener: listener,
	}

	form.Primitive = Modal(form.Layout, 70, 10)

	form.Layout.SetDirection(tview.FlexRow)
	form.Layout.AddItem(form.Info, 6, 0, false)
	form.Layout.AddItem(form.Form, 9, 1, true)
	form.Layout.AddItem(form.Warning, 1, 0, false)

	form.Info.SetText("Bichon needs to encrypt the sensitive GitForge API tokens, " +
		"but does not appear to be running in a desktop session " +
		"with the secret service key storage API available. " +
		"Please choose a password to use for API token encryption. " +
		"This will be need to be provided at future application startup.")
	form.Info.SetWordWrap(true)

	form.Warning.SetDynamicColors(true)

	form.Form.SetBorder(true).
		SetTitle("GitForge API token password")

	form.Form.AddPasswordField("Password", "", 20, '*',
		func(text string) {
			form.Password = text
		})
	form.Form.AddPasswordField("Password (again)", "", 20, '*',
		func(text string) {
			form.PasswordAgain = text
		})

	form.Form.AddDefaultButton("Confirm", func() {
		form.Form.SetFocus(0)
		if form.Password != form.PasswordAgain {
			form.Warning.SetText("[red]Passwords do not match[red]")
			return
		}
		if len(form.Password) < 8 {
			form.Warning.SetText("[red]Password must be at least 8 characters[red]")
			return
		}
		form.Listener.ChoosePasswordFormConfirm(form.Password)
	})

	return form
}
