// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type DetailPageListener interface {
	DetailPageQuit()
	DetailPageRefreshMergeRequest(mreq *model.MergeReq)
	DetailPageAddMergeReqComment(mreq *model.MergeReq, text string, standAlone bool, context *model.CommentContext)
	DetailPageAddMergeReqReply(mreq *model.MergeReq, thread, text string)
	DetailPageResolveMergeReqThread(mreq *model.MergeReq, thread string, resolved bool)
	DetailPageLoadMergeReqSeriesPatches(mreq *model.MergeReq, series *model.Series)
	DetailPageLoadMergeReqCommitDiffs(mreq *model.MergeReq, commit *model.Commit)
	DetailPageAcceptMergeReq(mreq *model.MergeReq)
	DetailPageApproveMergeReq(mreq *model.MergeReq)
	DetailPageUnapproveMergeReq(mreq *model.MergeReq)
	DetailPagePickVersion()
	DetailPageChangePatch(num int)
}

type DetailPage struct {
	*tview.Flex

	Application    *tview.Application
	Listener       DetailPageListener
	Patches        *tview.Table
	Header         *tview.Frame
	Patch          *model.Commit
	Content        *tview.TextView
	ContentLine    int
	ContentScroll  int
	ContentLen     int
	ContentRegions []PatchRegion
	CommentText    *tview.TextArea
	CommentForm    *CommentForm
	CommentFrame   *tview.Frame
	CommentEdit    bool
	CommentRegion  *PatchRegion
	CommentOffset  uint
	CommentShow    bool

	MergeReq *model.MergeReq

	Version int
}

func NewDetailPage(app *tview.Application, listener DetailPageListener) *DetailPage {
	patches := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.StyleDefault.
			Foreground(GetStyleColor(ELEMENT_MREQS_ACTIVE_TEXT)).
			Background(GetStyleColor(ELEMENT_MREQS_ACTIVE_FILL)).
			Attributes(GetStyleAttrMask(ELEMENT_MREQS_ACTIVE_ATTR))).
		SetFixed(1, 0)

	header := tview.NewFrame(patches).
		SetBorders(0, 0, 0, 0, 0, 0).
		AddText(fmt.Sprintf("[%s:%s:%s]",
			GetStyleColorName(ELEMENT_HEADER_TEXT),
			GetStyleColorName(ELEMENT_HEADER_FILL),
			GetStyleColorName(ELEMENT_HEADER_ATTR))+
			strings.Repeat(" ", 500),
			false, tview.AlignLeft, tcell.ColorWhite)

	content := tview.NewTextView().
		SetDynamicColors(true).
		SetRegions(true).
		SetTextFlags(true)

	layout := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(header, 7, 1, false).
		AddItem(content, 0, 1, false)

	page := &DetailPage{
		Flex: layout,

		Application: app,
		Listener:    listener,
		Patches:     patches,
		Header:      header,
		Content:     content,
		CommentShow: true,
	}

	commentForm := NewCommentForm(page)
	page.CommentForm = commentForm

	patches.SetSelectionChangedFunc(func(row, col int) {
		page.ContentLine = 0
		page.ContentScroll = 0
		page.switchPatch(row, col)
		page.Listener.DetailPageChangePatch(row)
	})

	return page
}

func (page *DetailPage) GetName() string {
	return "detail"
}

func (page *DetailPage) GetKeyShortcuts() string {
	return "q:Index r:Refresh c:Comment a:Approve A:Unapprove h:Hide comments m:Merge v:Version"
}

func (page *DetailPage) buildMergeReqRow(mreq *model.MergeReq, version int, series *model.Series) [7]*tview.TableCell {
	npatches := 0
	if series != nil {
		npatches = len(series.Patches)
	}

	return [7]*tview.TableCell{
		&tview.TableCell{
			Text:            fmt.Sprintf("%5s", fmt.Sprintf("#%d", mreq.ID)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Repo.NickName, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", mreq.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", tview.Escape(TrimEllipsisFront(mreq.Submitter.RealName, 20))),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%4s", fmt.Sprintf("v%d", version)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%7s", fmt.Sprintf("0/%d", npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(mreq.Title),
			Expansion:       1,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *DetailPage) buildMergeReqPatchRow(mreq *model.MergeReq, series *model.Series, idx int) [7]*tview.TableCell {
	patch := &series.Patches[idx]
	npatches := len(series.Patches)

	var marker string
	if idx == (len(series.Patches) - 1) {
		marker = "└─>  "
	} else {
		marker = "├─>  "
	}
	return [7]*tview.TableCell{
		&tview.TableCell{
			Text:            "",
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            marker,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", patch.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", tview.Escape(TrimEllipsisFront(patch.Author.Name, 20))),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            "",
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%7s", fmt.Sprintf("%d/%d", idx+1, npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(patch.Title),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *DetailPage) refreshMain(app *tview.Application, mreq *model.MergeReq) {
	log.Infof("Refreshing detail page main")
	if page.MergeReq == nil || !page.MergeReq.Equal(mreq) {
		log.Infof("Reset content line")
		page.ContentLine = 0
		page.ContentScroll = 0
		page.Version = 0
		page.Patches.Select(0, 0)
	} else {
		row, _ := page.Content.GetScrollOffset()
		page.ContentScroll = row
	}
	page.MergeReq = mreq
	page.updatePatches()
	page.updateContent()
}

func (page *DetailPage) Refresh(app *tview.Application, mreq *model.MergeReq) {
	log.Infof("Refreshing detail page queue")
	go app.QueueUpdateDraw(func() {
		page.refreshMain(app, mreq)
	})
}

func (page *DetailPage) SelectPatch(num int) {
	row, col := page.Patches.GetSelection()
	if row == num {
		return
	}
	if num < page.Patches.GetRowCount() {
		page.Patches.Select(num, col)
	}
}

func (page *DetailPage) SwitchVersion(version int) {
	log.Infof("Switch to version %d", version)
	if version > len(page.MergeReq.Versions) {
		return
	}
	page.Version = version
	page.updatePatches()
	page.updateContent()
}

func (page *DetailPage) Activate() {
	page.RemoveItem(page.CommentForm)
	page.Application.SetFocus(page.Content)
}

func (page *DetailPage) CommentFormSave(text string, standAlone bool) {
	if text != "" {
		if page.CommentRegion != nil {
			if page.CommentRegion.Thread == "" {
				context := page.getCommitContext(page.CommentRegion, page.CommentOffset)
				page.Listener.DetailPageAddMergeReqComment(page.MergeReq, text, standAlone, context)
			} else {
				page.Listener.DetailPageAddMergeReqReply(page.MergeReq, page.CommentRegion.Thread, text)
			}
		} else {
			page.Listener.DetailPageAddMergeReqComment(page.MergeReq, text, standAlone, nil)
		}
	}

	page.CommentFormCancel()
}

func (page *DetailPage) CommentFormCancel() {
	page.RemoveItem(page.CommentForm)
	page.Application.SetFocus(page.Content)
	page.CommentEdit = false
	page.CommentRegion = nil
}

func (page *DetailPage) moveTo(contentLine int) {
	page.ContentLine = contentLine
	if page.ContentLine < 1 {
		page.ContentLine = 1
	} else if page.ContentLine > page.ContentLen {
		page.ContentLine = page.ContentLen
	}
	page.Content.Highlight(fmt.Sprintf("l%d", page.ContentLine))
	page.Content.ScrollToHighlight()
}

func (page *DetailPage) moveCursor(delta int, fullPage bool) bool {
	if page.ContentLen == 0 {
		return false
	}

	var firstRegion, lastRegion int
	regionInfos := page.Content.GetVisibleHighlightRegions()
	for _, r := range regionInfos {
		fmt.Sscanf(r.ID, "l%d", &lastRegion)
		if firstRegion == 0 && r.FromY >= 0 {
			firstRegion = lastRegion
		}
	}

	var newContentLine int
	y, x := page.Content.GetScrollOffset()

	if fullPage {
		if delta < 0 {
			page.ContentLine = firstRegion
		} else {
			page.ContentLine = lastRegion
		}
		newContentLine = page.ContentLine + delta
		if newContentLine < 1 {
			newContentLine = 1
		} else if newContentLine > page.ContentLen {
			newContentLine = page.ContentLen
		}

		_, _, _, h := page.Content.GetInnerRect()
		delta *= h
	} else {
		newContentLine = page.ContentLine + delta
		if newContentLine < 1 || newContentLine > page.ContentLen {
			/* Reached the buffer top or bottom highlight. Still
			 * try to scroll in the delta direction, as there
			 * may be wrapped lines (technically, only at the
			 * bottom but let's behave the same at the top for
			 * consistency and more robust code).
			 */
			newContentLine = page.ContentLine
		} else if newContentLine >= firstRegion && newContentLine <= lastRegion {
			/* We're still within the visible area. Note this
			 * works also for no visible regions (an extremely
			 * long wrapped line). */
			page.ContentLine = newContentLine
			page.Content.Highlight(fmt.Sprintf("l%d", page.ContentLine))
			return true
		}
	}

	/* We need to scroll. Highlight the expected new line. */
	page.Content.ScrollTo(y+delta, x)
	page.Content.Highlight(fmt.Sprintf("l%d", newContentLine))
	if !page.Content.IsHighlightVisible() && newContentLine != page.ContentLine {
		/* Even after scrolling, the highlight is not visible. This
		 * means there's a wrapped line at the beginning or end of
		 * screen. Move the highlight back. */
		page.Content.Highlight(fmt.Sprintf("l%d", page.ContentLine))
	} else {
		page.ContentLine = newContentLine
	}

	return true
}

func (page *DetailPage) findRegion() (*PatchRegion, uint) {
	ids := page.Content.GetHighlights()

	if len(ids) != 1 {
		log.Infof("No regions highlighted")
		return nil, 0
	}

	if !strings.HasPrefix(ids[0], "l") {
		log.Infof("Unexpected region id '%s'", ids[0])
		return nil, 0
	}

	id, err := strconv.Atoi(ids[0][1:])
	if err != nil {
		log.Infof("Malformed region id '%s'", ids[0][1:])
		return nil, 0
	}

	for idx, _ := range page.ContentRegions {
		region := &page.ContentRegions[idx]
		if id >= region.IDStart && id <= region.IDEnd {
			log.Infof("Found %d in region (%d-%d) type %d file old %s#%d new %s#%d",
				id, region.IDStart, region.IDEnd, region.Type,
				region.OldFile, region.OldLine, region.NewFile, region.NewLine)
			return region, uint(id - region.IDStart)
		}
	}

	log.Infof("No matching region ID")
	return nil, 0
}

func (page *DetailPage) getCommitContext(region *PatchRegion, offset uint) *model.CommentContext {
	ver := page.MergeReq.Versions[len(page.MergeReq.Versions)-1]
	parent := ver.BaseHash
	for _, patch := range ver.Patches {
		if patch.Hash == page.Patch.Hash {
			break
		}
		parent = patch.Hash
	}

	/* Note that even if OldLine or NewLine are zero
	 * (ie removed or added lines), we must always pass
	 * OldFile and NewFile names. If either filename is
	 * missing then the github web UI won't display the comment
	 */
	oldline := region.OldLine
	if oldline != 0 {
		oldline += offset
	}
	newline := region.NewLine
	if newline != 0 {
		newline += offset
	}

	log.Infof("Commit context is old=%s:%d new=%s:%d base=%s start=%s head=%s",
		region.OldFile, oldline, region.NewFile, newline, parent, parent,
		page.Patch.Hash)
	return &model.CommentContext{
		BaseHash:  parent,
		StartHash: parent,
		HeadHash:  page.Patch.Hash,
		OldFile:   region.OldFile,
		OldLine:   oldline,
		NewFile:   region.NewFile,
		NewLine:   newline,
	}
}

func (page *DetailPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	var handled bool
	event, handled = page.CommentForm.HandleFocusNav(page.Application, event, page.Content)
	if handled {
		return event
	}

	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'k': // up
			if !page.moveCursor(-1, false) {
				return event
			}
		case 'j': // down
			if !page.moveCursor(1, false) {
				return event
			}
		case 'g': // home
			if page.ContentLen != 0 {
				page.moveTo(1)
			} else {
				return event
			}
		case 'G': // home
			if page.ContentLen != 0 {
				page.moveTo(page.ContentLen)
			} else {
				return event
			}
		case 'q':
			page.Listener.DetailPageQuit()
		case 'c':
			log.Infof("Maybe activating comment")
			showform := false
			page.CommentRegion = nil
			page.CommentOffset = 0
			var ctx *model.CommentContext
			reply := -1
			allowStandalone := false
			if page.Patch != nil {
				region, offset := page.findRegion()
				if region != nil {
					page.CommentRegion = region
					page.CommentOffset = offset
					var adjustLines uint
					if page.CommentRegion.Thread != "" {
						// Region points to an existing comment, line numbers
						// reflect correct context, no adjustment needed
						reply = page.CommentRegion.ThreadIdx
					} else {
						// Region points to the diff, line numbers point to
						// start of the diff hunk, so we need to offset them
						adjustLines = page.CommentOffset
					}
					ctx = page.getCommitContext(page.CommentRegion, adjustLines)

					showform = true
				}
			} else {
				// XXX allow replies to top level comments
				allowStandalone = true
				showform = true
			}

			if showform {
				page.AddItem(page.CommentForm, 20, 0, true)
				page.CommentForm.Activate(page.Application, ctx, reply, allowStandalone)
				page.CommentEdit = true
			}
		case 'm':
			if page.MergeReq != nil {
				page.Listener.DetailPageAcceptMergeReq(page.MergeReq)
			}
		case 'a':
			if page.MergeReq != nil {
				page.Listener.DetailPageApproveMergeReq(page.MergeReq)
			}
		case 'A':
			if page.MergeReq != nil {
				page.Listener.DetailPageUnapproveMergeReq(page.MergeReq)
			}
		case 'r':
			if page.MergeReq != nil {
				page.Listener.DetailPageRefreshMergeRequest(page.MergeReq)
			}
		case 'v':
			if page.MergeReq != nil {
				page.Listener.DetailPagePickVersion()
			}
		case 's':
			log.Infof("Maybe resolving comment")
			var thread *model.CommentThread
			if page.Patch != nil {
				region, _ := page.findRegion()
				if region != nil && region.Thread != "" {
					thread = &page.MergeReq.Threads[region.ThreadIdx]
					if len(thread.Comments) == 0 ||
						!thread.Comments[0].Resolvable {
						log.Info("Not resolvable")
						thread = nil
					}
				} else {
					log.Info("No region / thread")
				}
			} else {
				log.Infof("No patch")
				/// resolve cover letter comments
			}

			if thread != nil {
				log.Infof("Ok try")
				page.Listener.DetailPageResolveMergeReqThread(
					page.MergeReq, thread.ID, !thread.Comments[0].Resolved)
			}
		case 'h':
			page.CommentShow = !page.CommentShow
			page.updateContent()
		default:
			return event
		}

	case tcell.KeyTab:
		if page.CommentEdit {
			page.CommentForm.SetFocus(page.Application, false)
		}
	case tcell.KeyBacktab:
		if page.CommentEdit {
			page.CommentForm.SetFocus(page.Application, true)
		}
	case tcell.KeyLeft:
		if !page.CommentEdit {
			row, col := page.Patches.GetSelection()
			if row < 0 {
				break
			}
			if row > 0 {
				page.Patches.Select(row-1, col)
			}
			if row < (page.Patches.GetRowCount() - 3) {
				row, col = page.Patches.GetOffset()
				page.Patches.SetOffset(row-1, col)
			}
		}

	case tcell.KeyRight:
		if !page.CommentEdit {
			row, col := page.Patches.GetSelection()
			if row < 0 {
				break
			}
			if row < (page.Patches.GetRowCount() - 1) {
				page.Patches.Select(row+1, col)
			}
			if row > 2 {
				row, col = page.Patches.GetOffset()
				page.Patches.SetOffset(row+1, col)
			}
		}

	case tcell.KeyPgUp, tcell.KeyCtrlB:
		if !page.moveCursor(-1, true) {
			return event
		}
	case tcell.KeyPgDn, tcell.KeyCtrlF:
		if !page.moveCursor(1, true) {
			return event
		}
	case tcell.KeyUp:
		if !page.moveCursor(-1, false) {
			return event
		}
	case tcell.KeyDown:
		if !page.moveCursor(1, false) {
			return event
		}
	case tcell.KeyHome:
		if page.ContentLen != 0 {
			page.moveTo(1)
		} else {
			return event
		}
	case tcell.KeyEnd:
		if page.ContentLen != 0 {
			page.moveTo(page.ContentLen)
		} else {
			return event
		}
	case tcell.KeyEscape:
		page.Listener.DetailPageQuit()
	default:
		return event
	}

	return nil
}

func (page *DetailPage) updatePatches() {
	page.Patches.Clear()

	mreq := page.MergeReq
	if mreq == nil {
		return
	}

	cells := make([][7]*tview.TableCell, 0)

	var version int
	var series *model.Series
	if page.Version == 0 {
		version = len(mreq.Versions)
	} else {
		version = page.Version
	}
	if version != 0 {
		series = &mreq.Versions[version-1]
	}
	cells = append(cells, page.buildMergeReqRow(mreq, version, series))
	if series != nil {
		for idx, _ := range series.Patches {
			cells = append(cells, page.buildMergeReqPatchRow(mreq, series, idx))
		}
	}

	for idx, row := range cells {
		page.Patches.SetCell(idx, 0, row[0])
		page.Patches.SetCell(idx, 1, row[1])
		page.Patches.SetCell(idx, 2, row[2])
		page.Patches.SetCell(idx, 3, row[3])
		page.Patches.SetCell(idx, 4, row[4])
		page.Patches.SetCell(idx, 5, row[5])
		page.Patches.SetCell(idx, 6, row[6])
	}
	page.Patches.SetOffset(0, 0)
	row, col := page.Patches.GetSelection()
	if row >= page.Patches.GetRowCount() {
		row = page.Patches.GetRowCount() - 1
		page.Patches.Select(row, col)
	}
}

func (page *DetailPage) buildRegions(data string) string {
	lines := strings.Split(data, "\n")
	for idx, _ := range lines {
		lines[idx] = fmt.Sprintf("[\"n%d\"]", idx) + lines[idx] + "[\"\"]"
	}
	return strings.Join(lines, "\n")
}

func (page *DetailPage) formatCoverLetter() string {
	colorDefault := fmt.Sprintf("[%s:%s:-]",
		GetStyleColorName(ELEMENT_PRIMARY_TEXT),
		GetStyleColorName(ELEMENT_PRIMITIVE_FILL))
	colorHeader :=
		GetStyleMarker(
			ELEMENT_SUMMARY_HEADER_TEXT,
			ELEMENT_SUMMARY_HEADER_FILL,
			ELEMENT_SUMMARY_HEADER_ATTR)
	colorTitle :=
		GetStyleMarker(
			ELEMENT_SUMMARY_TITLE_TEXT,
			ELEMENT_SUMMARY_TITLE_FILL,
			ELEMENT_SUMMARY_TITLE_ATTR)
	colorDate :=
		GetStyleMarker(
			ELEMENT_SUMMARY_DATE_TEXT,
			ELEMENT_SUMMARY_DATE_FILL,
			ELEMENT_SUMMARY_DATE_ATTR)

	mreq := page.MergeReq
	body := colorHeader + "Merge Request: " +
		colorTitle + tview.Escape(mreq.Title) +
		colorHeader + " on " +
		colorDate + mreq.CreatedAt.Format(time.RFC1123) +
		colorDefault +
		"\n\n"

	body += GetStyleAltStr(OPTION_ALLOW_EMOJI, "➰ ", "@ ")
	switch mreq.State {
	case model.STATE_OPENED:
		body += "open"
		if mreq.MergeStatus == "can_be_merged" {
			if mreq.MergeAfterPipeline {
				body += " (merging when pipeline succeeds)"
			} else {
				body += " (mergeable)"
			}
		} else {
			body += " (needs rebase)"
		}
	case model.STATE_CLOSED:
		body += "closed"
	case model.STATE_MERGED:
		body += "merged"
	case model.STATE_LOCKED:
		body += "locked"
	}
	body += " " + mreq.ReviewURL()
	body += "\n"
	if len(mreq.Labels) > 0 {
		body += GetStyleAltStr(OPTION_ALLOW_EMOJI, "🔖", "Labels:")
		for idx, l := range mreq.Labels {
			if idx > 0 {
				body += ","
			}
			body += " " + l
		}
		body += "\n"
	}

	if mreq.Assignee != nil {
		body += fmt.Sprintf(GetStyleAltStr(OPTION_ALLOW_EMOJI, "🕵  %s", "Assignee: %s"),
			mreq.Assignee.RealName) + "\n"
	}

	if mreq.Approvals.Required > 0 || len(mreq.Approvals.ApprovedBy) > 0 {
		body += fmt.Sprintf(GetStyleAltStr(OPTION_ALLOW_EMOJI, "✅ %d/%d", "Approvals: %d/%d"),
			mreq.Approvals.Required-mreq.Approvals.Remaining, mreq.Approvals.Required)
		var approvers []string
		for _, acct := range mreq.Approvals.ApprovedBy {
			approvers = append(approvers, acct.RealName)
		}
		if len(approvers) > 0 {
			body += " (" + strings.Join(approvers, ", ") + ")"
		}
		body += "\n"
	}

	if mreq.UpVotes != 0 || mreq.DownVotes != 0 {
		body += fmt.Sprintf(GetStyleAltStr(OPTION_ALLOW_EMOJI, "👍 %d 👎 %d", "Votes: +%d -%d"),
			mreq.UpVotes, mreq.DownVotes) + "\n"
	}
	body += "\n"

	body += fmt.Sprintf("  git fetch %s %s\n", mreq.Repo.Remote, mreq.OriginRef())
	body += fmt.Sprintf("  git checkout -b %s FETCH_HEAD\n", mreq.LocalBranch(page.Version))

	body += "\n"

	body += tview.Escape(mreq.Description) + "\n\n\n"

	region := 0
	var idxs []int
	for idx, _ := range mreq.Threads {
		idxs = append(idxs, idx)
	}
	lines, _ := FormatThreads(mreq.Threads, idxs, true, false, &region, "")

	body += strings.Join(lines, "\n") + "\n"

	return body
}

func (page *DetailPage) formatPatch(ver *model.Series, num int) (*model.Commit, []string, []PatchRegion) {
	if num >= len(ver.Patches) {
		return nil, []string{
			fmt.Sprintf("Commit %d does exist in this version", num),
		}, []PatchRegion{}
	}

	patch := &ver.Patches[num]
	if patch.Metadata.Partial {
		page.Listener.DetailPageLoadMergeReqCommitDiffs(page.MergeReq, patch)
	}

	lines, regions, err := FormatCommit(patch, ver, page.CommentShow, page.MergeReq.Threads)
	if err != nil {
		return nil, []string{
			fmt.Sprintf("Unable to format commit %s", err),
		}, []PatchRegion{}
	}
	return patch, lines, regions
}

func (page *DetailPage) switchPatch(row, col int) {
	if page.MergeReq == nil {
		page.ContentLine = 0
		page.Content.SetText("")
		return
	}

	var ver *model.Series
	if page.Version == 0 {
		ver = &page.MergeReq.Versions[len(page.MergeReq.Versions)-1]
	} else {
		ver = &page.MergeReq.Versions[page.Version-1]
	}
	log.Infof("Version %d has %d patches", page.Version, len(ver.Patches))
	if ver.Metadata.Partial {
		page.Listener.DetailPageLoadMergeReqSeriesPatches(page.MergeReq, ver)
	}

	log.Infof("Re-rendering patches %d", row)
	if row <= 0 {
		page.Patch = nil
		page.ContentLen = 0
		page.ContentRegions = []PatchRegion{}
		page.Content.SetText(page.formatCoverLetter())
		page.Content.ScrollTo(page.ContentScroll, 1)
	} else {
		patch, lines, regions := page.formatPatch(ver, row-1)
		page.Patch = patch
		page.ContentLen = len(lines)
		page.ContentRegions = regions
		page.Content.SetText(strings.Join(lines, "\n") + "\n")
		page.moveTo(page.ContentLine)
	}
}

func (page *DetailPage) updateContent() {
	row, col := page.Patches.GetSelection()
	page.switchPatch(row, col)
}
