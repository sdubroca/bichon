// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"sort"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type ProjectsPageListener interface {
	ProjectsPageQuit()
	ProjectsPageAddRepo()
	ProjectsPageEditRepo(repo model.Repo)
}

type ProjectsPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    ProjectsPageListener
	Projects    *tview.Table
}

func NewProjectsPage(app *tview.Application, listener ProjectsPageListener) *ProjectsPage {
	projects := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.StyleDefault.
			Foreground(GetStyleColor(ELEMENT_PROJECTS_ACTIVE_TEXT)).
			Background(GetStyleColor(ELEMENT_PROJECTS_ACTIVE_FILL)).
			Attributes(GetStyleAttrMask(ELEMENT_PROJECTS_ACTIVE_ATTR)))

	layout := tview.NewFrame(projects).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &ProjectsPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		Projects:    projects,
	}

	return page
}

func (page *ProjectsPage) GetName() string {
	return "projects"
}

func (page *ProjectsPage) GetKeyShortcuts() string {
	return "q:Back to index a:Add project e:Edit project"
}

func (page *ProjectsPage) Refresh(app *tview.Application, repos []model.Repo) {
	go app.QueueUpdateDraw(func() {
		page.refreshMain(app, repos)
	})
}

func (page *ProjectsPage) buildRepoRow(repo *model.Repo) [2]*tview.TableCell {
	return [2]*tview.TableCell{
		&tview.TableCell{
			Text:            repo.NickName,
			Reference:       repo,
			Color:           GetStyleColor(ELEMENT_PROJECTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_PROJECTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            string(repo.State),
			Color:           GetStyleColor(ELEMENT_PROJECTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_PROJECTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *ProjectsPage) refreshMain(app *tview.Application, repos model.Repos) {
	page.Projects.Clear()

	sort.Sort(repos)
	for idx, _ := range repos {
		repo := &repos[idx]
		row := page.buildRepoRow(repo)

		for col, val := range row {
			page.Projects.SetCell(idx, col, val)
		}
	}
}

func (page *ProjectsPage) Activate() {
	page.Application.SetFocus(page.Projects)
}

func (page *ProjectsPage) getSelectedRepo() *model.Repo {
	if page.Projects.GetRowCount() == 0 {
		return nil
	}
	row, _ := page.Projects.GetSelection()
	if row < 0 {
		return nil
	}

	cell := page.Projects.GetCell(row, 0)
	log.Infof("Selected %d %p", row, cell)
	ref := cell.GetReference()

	if ref == nil {
		return nil
	}

	mreq, ok := ref.(*model.Repo)
	if !ok {
		return nil
	}

	return mreq
}

func (page *ProjectsPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'q':
			page.Listener.ProjectsPageQuit()
		case 'a':
			page.Listener.ProjectsPageAddRepo()
		case 'e':
			repo := page.getSelectedRepo()
			if repo != nil {
				page.Listener.ProjectsPageEditRepo(*repo)
			}
		default:
			return event
		}
	case tcell.KeyEscape:
		page.Listener.ProjectsPageQuit()
	default:
		return event
	}

	return nil
}
