// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type IndexPageListener interface {
	IndexPageQuit()
	IndexPageViewMergeRequest(mreq model.MergeReq)
	IndexPageViewProjects()
	IndexPageViewReports()
	IndexPageRefreshMergeRequests()
	IndexPagePickSort()
	IndexPagePickFilter()
	IndexPagePickQuickFilter()
}

type IndexPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    IndexPageListener
	MergeReqs   *tview.Table
}

func NewIndexPage(app *tview.Application, listener IndexPageListener) *IndexPage {
	mreqs := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.StyleDefault.
			Foreground(GetStyleColor(ELEMENT_MREQS_ACTIVE_TEXT)).
			Background(GetStyleColor(ELEMENT_MREQS_ACTIVE_FILL)).
			Attributes(GetStyleAttrMask(ELEMENT_MREQS_ACTIVE_ATTR)))

	layout := tview.NewFrame(mreqs).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &IndexPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		MergeReqs:   mreqs,
	}

	return page
}

func (page *IndexPage) GetName() string {
	return "index"
}

func (page *IndexPage) GetKeyShortcuts() string {
	return "q:Quit r:Refresh p:Projects s:Sort f:Filter m:Filter string t:Reports"
}

func (page *IndexPage) buildMergeReqRow(mreq *model.MergeReq) [8]*tview.TableCell {
	nversions := len(mreq.Versions)
	npatches := 0
	if nversions > 0 {
		version := mreq.Versions[len(mreq.Versions)-1]
		npatches = len(version.Patches)
	}

	status := mreq.Metadata.Status
	if status == model.STATUS_READ {
		status = ""
	}
	if len(status) > 0 {
		status = status[0:1]
	}

	return [8]*tview.TableCell{
		&tview.TableCell{
			Text:            strings.ToUpper(string(status)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Reference:       mreq,
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%5s", fmt.Sprintf("#%d", mreq.ID)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Repo.NickName, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", mreq.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", tview.Escape(TrimEllipsisFront(mreq.Submitter.RealName, 20))),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%4s", fmt.Sprintf("v%d", nversions)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%-7s", fmt.Sprintf("0/%d", npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(mreq.Title),
			Expansion:       1,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *IndexPage) refreshMain(app *tview.Application, mreqs []*model.MergeReq) {
	log.Infof("Refreshing index page main")
	page.updateMergeReqs(mreqs)
}

func (page *IndexPage) Refresh(app *tview.Application, mreqs []*model.MergeReq) {
	log.Infof("Refreshing index page queue")
	go app.QueueUpdateDraw(func() {
		page.refreshMain(app, mreqs)
	})
}

func (page *IndexPage) Activate() {
	page.Application.SetFocus(page.MergeReqs)
}

func (page *IndexPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'q':
			page.Listener.IndexPageQuit()
		case 'r':
			page.Listener.IndexPageRefreshMergeRequests()

		case 'p':
			page.Listener.IndexPageViewProjects()
		case 't':
			page.Listener.IndexPageViewReports()
		case 's':
			page.Listener.IndexPagePickSort()
		case 'f':
			page.Listener.IndexPagePickFilter()
		case 'm':
			page.Listener.IndexPagePickQuickFilter()
		default:
			return event
		}

	case tcell.KeyCR:
		mreq := page.GetSelectedMergeRequest()
		if mreq != nil {
			page.Listener.IndexPageViewMergeRequest(*mreq)
		}

	case tcell.KeyTab:
		page.SelectNextNewishMergeRequest()

	default:
		return event
	}

	return nil
}

func (page *IndexPage) SelectNextNewishMergeRequest() {
	currow, _ := page.MergeReqs.GetSelection()
	log.Infof("Row selected %d", currow)

	maybeSelect := func(row int, status model.MergeReqStatus) bool {
		cell := page.MergeReqs.GetCell(row, 0)

		ref := cell.GetReference()

		if ref == nil {
			return false
		}

		mreq, ok := ref.(*model.MergeReq)
		if !ok {
			return false
		}

		if mreq.Metadata.Status == status {
			page.MergeReqs.Select(row, 0)
			log.Infof("Cell selected mreq %p %d:%s", mreq, mreq.ID, mreq.Title)
			return true
		}
		return false
	}

	statusCheck := []model.MergeReqStatus{
		model.STATUS_NEW,
		model.STATUS_UPDATED,
		model.STATUS_OLD,
	}

	for _, status := range statusCheck {
		for i := currow + 1; i < page.MergeReqs.GetRowCount(); i++ {
			if maybeSelect(i, status) {
				return
			}
		}

		for i := 0; i < currow; i++ {
			if maybeSelect(i, status) {
				return
			}
		}
	}
}

func (page *IndexPage) GetSelectedMergeRequest() *model.MergeReq {
	if page.MergeReqs.GetRowCount() == 0 {
		return nil
	}
	row, _ := page.MergeReqs.GetSelection()
	log.Infof("Row selected %d", row)
	if row == -1 {
		return nil
	}
	cell := page.MergeReqs.GetCell(row, 0)

	ref := cell.GetReference()

	if ref == nil {
		return nil
	}

	mreq, ok := ref.(*model.MergeReq)
	if !ok {
		return nil
	}
	log.Infof("Cell selected mreq %p %d:%s", mreq, mreq.ID, mreq.Title)
	return mreq
}

func (page *IndexPage) updateMergeReqs(mreqs []*model.MergeReq) {
	selectedMreq := page.GetSelectedMergeRequest()
	selectedIdx := -1
	cells := make([][8]*tview.TableCell, 0)
	for idx, mreq := range mreqs {
		cells = append(cells, page.buildMergeReqRow(mreq))
		if selectedMreq != nil && mreq.Equal(selectedMreq) {
			selectedIdx = idx
		}
	}

	page.MergeReqs.Clear()
	for idx, row := range cells {
		for col, _ := range row {
			page.MergeReqs.SetCell(idx, col, row[col])
		}
	}

	if selectedIdx != -1 {
		page.MergeReqs.Select(selectedIdx, 0)
	} else if len(mreqs) > 0 {
		page.MergeReqs.Select(0, 0)
	} else {
		page.MergeReqs.Select(-1, 0)
	}
}
