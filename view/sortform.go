// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"strings"

	"gitlab.com/bichon-project/bichon/model/sorter"
	"gitlab.com/bichon-project/tview"
)

type SortFormListener interface {
	SortFormConfirm(info sorter.SorterInfo)
	SortFormCancel()
}

var (
	sortFormTerms = [][]string{
		[]string{"project", "id"},
		[]string{"age"},
		[]string{"activity"},
		[]string{"title", "project", "id"},
		[]string{"realname", "project", "id"},
	}
)

type SortForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener SortFormListener

	Order   int
	Reverse bool
}

func (form *SortForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.SortFormCancel()
}

func NewSortForm(listener SortFormListener) *SortForm {

	form := &SortForm{
		Form:     tview.NewForm(),
		Listener: listener,
	}

	form.Primitive = Modal(form.Form, 30, 9)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.SetBorder(true).
		SetTitle("Merge request sorting")
	form.Form.AddDropDown("Field",
		[]string{
			"Project",
			"Age",
			"Activity",
			"Title",
			"Author",
		},
		1,
		func(val string, idx int) {
			form.Order = idx
		})
	form.Form.AddCheckbox("Reverse", false,
		func(checked bool) {
			form.Reverse = checked
		})

	form.Form.AddDefaultButton("Apply", func() {
		form.Form.SetFocus(0)
		info := form.getMergeReqSorter()
		form.Listener.SortFormConfirm(info)
	})

	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *SortForm) getMergeReqSorter() sorter.SorterInfo {
	terms := sortFormTerms[form.Order]

	var expr []string
	var reverse string
	if form.Reverse {
		reverse = "! "
	}
	for idx, _ := range terms {
		expr = append(expr, reverse+terms[idx])
	}

	return sorter.BuildSorter(strings.Join(expr, ", "))
}
