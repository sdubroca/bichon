// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"

	"gitlab.com/bichon-project/tview"
)

type PasswordFormListener interface {
	PasswordFormConfirm(password string) error
}
type PasswordForm struct {
	tview.Primitive

	Layout   *tview.Flex
	Info     *tview.TextView
	Form     *tview.Form
	Warning  *tview.TextView
	Listener PasswordFormListener

	Password string
}

func NewPasswordForm(listener PasswordFormListener) *PasswordForm {

	form := &PasswordForm{
		Form:     tview.NewForm(),
		Info:     tview.NewTextView(),
		Layout:   tview.NewFlex(),
		Warning:  tview.NewTextView(),
		Listener: listener,
	}

	form.Primitive = Modal(form.Layout, 70, 10)

	form.Layout.SetDirection(tview.FlexRow)
	form.Layout.AddItem(form.Info, 2, 0, false)
	form.Layout.AddItem(form.Form, 7, 1, true)
	form.Layout.AddItem(form.Warning, 1, 0, false)

	form.Info.SetText("Please enter the password previously provided " +
		"for API token encryption.")
	form.Info.SetWordWrap(true)

	form.Warning.SetDynamicColors(true)

	form.Form.SetBorder(true).
		SetTitle("GitForge API token password")

	form.Form.AddPasswordField("Password", "", 20, '*',
		func(text string) {
			form.Password = text
		})

	form.Form.AddDefaultButton("Confirm", func() {
		form.Form.SetFocus(0)
		err := form.Listener.PasswordFormConfirm(form.Password)
		if err != nil {
			form.Warning.SetText(fmt.Sprintf("[red]%s[red]", err))
		}
	})

	return form
}
