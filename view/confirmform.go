// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/tview"
)

type ConfirmFormListener interface {
	ConfirmFormResult(confirmed, dontaskagain bool)
}
type ConfirmForm struct {
	tview.Primitive

	Layout   *tview.Flex
	Info     *tview.TextView
	Form     *tview.Form
	Listener ConfirmFormListener

	DontAskAgain bool
}

func NewConfirmForm(msg string, listener ConfirmFormListener) *ConfirmForm {

	form := &ConfirmForm{
		Form:     tview.NewForm(),
		Info:     tview.NewTextView(),
		Layout:   tview.NewFlex(),
		Listener: listener,
	}

	form.Primitive = Modal(form.Layout, 70, 10)

	form.Layout.SetBorder(true)
	form.Layout.SetDirection(tview.FlexRow)
	form.Layout.SetTitle("Confirmation required")
	form.Layout.SetBackgroundColor(tview.Styles.PrimitiveBackgroundColor)
	form.Layout.AddItem(form.Info, 2, 0, false)
	form.Layout.AddItem(form.Form, 5, 1, true)

	form.Info.SetText(msg)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.AddCheckbox("Don't ask this again", false, func(checked bool) {
		form.DontAskAgain = checked
	})
	form.Form.AddDefaultButton("Confirm", func() {
		form.Form.SetFocus(0)
		form.Listener.ConfirmFormResult(true, form.DontAskAgain)
	})

	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *ConfirmForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.ConfirmFormResult(false, form.DontAskAgain)
}
