// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"
	"unicode"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/model/filter"
	"gitlab.com/bichon-project/tview"
)

type FilterFormListener interface {
	FilterFormConfirm(info filter.FilterInfo)
	FilterFormCancel()
}

type FilterForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener FilterFormListener
	Projects *tview.DropDown

	StateOpen   bool
	StateClosed bool
	StateLocked bool
	StateMerged bool

	StatusNew     bool
	StatusUpdated bool
	StatusOld     bool
	StatusRead    bool

	Project         string
	AgeUnit         string
	AgeVal          time.Duration
	Author          string
	AuthorFixedCase bool
	Label           string
	WIP             *bool
}

func (form *FilterForm) addCheckbox(label, text string, val bool, changed func(checked bool)) {
	cbox := tview.NewCheckbox().
		SetLabel(label).
		SetMessage(text).
		SetChecked(val).
		SetChangedFunc(changed)
	form.Form.AddFormItem(cbox)
}

func (form *FilterForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.FilterFormCancel()
}

func NewFilterForm(listener FilterFormListener) *FilterForm {

	form := &FilterForm{
		Form:     tview.NewForm(),
		Listener: listener,
		Projects: tview.NewDropDown(),

		StateOpen: true,
	}

	form.Primitive = Modal(form.Form, 50, 35)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.SetBorder(true).
		SetTitle("Merge request filtering")

	form.Form.AddFormItem(form.Projects)
	form.Projects.SetLabel("Project")
	form.refreshMain([]model.Repo{})

	form.addCheckbox("State", "Opened", true, func(val bool) {
		form.StateOpen = val
	})
	form.addCheckbox("", "Closed", false, func(val bool) {
		form.StateClosed = val
	})
	form.addCheckbox("", "Merged", false, func(val bool) {
		form.StateMerged = val
	})
	form.addCheckbox("", "Locked", false, func(val bool) {
		form.StateLocked = val
	})

	form.addCheckbox("Status", "New", false, func(val bool) {
		form.StatusNew = val
	})
	form.addCheckbox("", "Updated", false, func(val bool) {
		form.StatusUpdated = val
	})
	form.addCheckbox("", "Old", false, func(val bool) {
		form.StatusOld = val
	})
	form.addCheckbox("", "Read", false, func(val bool) {
		form.StatusRead = val
	})

	form.Form.AddInputField("Age", "", 10,
		func(text string, char rune) bool {
			return unicode.IsDigit(char)
		},
		func(text string) {
			val, _ := strconv.Atoi(text)
			form.AgeVal = time.Duration(val)
		})
	form.Form.AddDropDown("", []string{
		"unlimited",
		"mins",
		"hours",
		"days",
		"weeks",
		"months",
		"years",
	}, 0, func(option string, idx int) {
		if idx == 0 {
			form.AgeUnit = ""
		} else {
			form.AgeUnit = option
		}
	})

	form.Form.AddInputField("Author", "", 20, nil,
		func(text string) {
			form.Author = text
		})
	form.addCheckbox("", "Case sensitive", false, func(val bool) {
		form.AuthorFixedCase = val
	})
	form.Form.AddInputField("Label", "", 20, nil,
		func(text string) {
			form.Label = text
		})
	form.Form.AddDropDown("WIP", []string{
		"any",
		"exclude",
		"include",
	}, 0, func(option string, idx int) {
		if idx == 0 {
			form.WIP = nil
		} else if idx == 1 {
			wip := false
			form.WIP = &wip
		} else if idx == 2 {
			wip := true
			form.WIP = &wip
		}
	})

	form.Form.AddDefaultButton("Apply", func() {
		form.Form.SetFocus(0)
		info := form.getMergeReqFilter()
		form.Listener.FilterFormConfirm(info)
	})
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *FilterForm) Refresh(app *tview.Application, repos []model.Repo) {
	go app.QueueUpdateDraw(func() {
		form.refreshMain(repos)
	})
}

func (form *FilterForm) refreshMain(repos model.Repos) {
	projects := []string{"-any-"}
	sort.Sort(repos)
	for _, repo := range repos {
		if repo.State != model.RepoStateHidden {
			projects = append(projects, repo.NickName)
		}
	}
	form.Projects.SetOptions(projects,
		func(text string, idx int) {
			if idx == 0 {
				form.Project = ""
			} else {
				form.Project = text
			}
		})
	form.Projects.SetCurrentOption(0)
}

func (form *FilterForm) getMergeReqFilter() filter.FilterInfo {
	var expr []string

	if form.Project != "" {
		expr = append(expr, fmt.Sprintf("~p %s", form.Project))
	}

	var stateFilter []string
	if form.StateOpen {
		stateFilter = append(stateFilter, "~s open")
	}
	if form.StateClosed {
		stateFilter = append(stateFilter, "~s closed")
	}
	if form.StateMerged {
		stateFilter = append(stateFilter, "~s merged")
	}
	if form.StateLocked {
		stateFilter = append(stateFilter, "~s locked")
	}

	if len(stateFilter) != 0 {
		expr = append(expr, "( "+strings.Join(stateFilter, " | ")+" )")
	}

	var statusFilter []string
	if form.StatusNew {
		statusFilter = append(statusFilter, "~m new")
	}
	if form.StatusUpdated {
		statusFilter = append(statusFilter, "~m updated")
	}
	if form.StatusOld {
		statusFilter = append(statusFilter, "~m old")
	}
	if form.StatusRead {
		statusFilter = append(statusFilter, "~m read")
	}

	if len(statusFilter) != 0 {
		expr = append(expr, "( "+strings.Join(statusFilter, " | ")+" )")
	}

	if form.AgeUnit != "" && form.AgeVal != 0 {
		expr = append(expr, fmt.Sprintf("~a %d %s", form.AgeVal, form.AgeUnit))
	}

	if form.Author != "" {
		if form.AuthorFixedCase {
			expr = append(expr, fmt.Sprintf("~n %s /g", form.Author))
		} else {
			expr = append(expr, fmt.Sprintf("~n %s /g /i", form.Author))
		}
	}

	if form.Label != "" {
		expr = append(expr, fmt.Sprintf("~l %s", form.Label))
	}

	if form.WIP != nil {
		if *form.WIP {
			expr = append(expr, "~w")
		} else {
			expr = append(expr, "! ~w")
		}
	}

	return filter.BuildFilter(strings.Join(expr, " & "))
}
