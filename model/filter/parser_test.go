// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package filter

import (
	"testing"
)

func TestParse(t *testing.T) {
	filters := []string{
		"~p bichon",
		"~p bichon*",
		"~p */bichon",
		"~p bichon-project/bichon",
		"~p libvirt/libvirt-*",
		"~n Fred",
		"~n \"Fred Blogs\"",
		"~n 'Fred Blogs'",
		"~n Fred /i",
		"~n \"Fred*\" /g",
		"~n 'Fred*' /g /i",
		"~u fredblogs",
		"~v 3",
		"~v == 3",
		"~v >= 3",
		"~v < 3",
		"~vn Fred* /g",
		"~vu fred /i",
		"~a 1 month",
		"~t 1 month",
		"~t 2 months",
		"~s o",
		"!~p bichon",
		"~s open",
		"~m old",
		"~p bichon & ~n \"Fred Blogs\"",
		"~p bichon & ! ~u fredblogs",
		"~p bichon & ( ~u fredblogs | ~u joerandom) & ~s open",
		"(~p bichon | ~p libvirt) & ~t 1 month",
		"(~p bichon | ~p libvirt) & ~a 1 y",
		"(~p bichon | ~p libvirt) & ~t 1 month & ~m new",
		"!(~p bichon | ~p libvirt) & ~t 1 month & ~m new",
		"~p bichon & ~w",
		"~p bichon & ! ~w",
	}

	for _, filter := range filters {
		expr, err := NewExpression(filter)
		if err != nil {
			t.Fatalf("cannot parse '%s': %s", filter, err)
		}

		_ = expr.BuildFilter()
	}
}

func TestStrings(t *testing.T) {
	filters := []string{
		"~p bichon",
		"~p 'bichon'",
		"~p \"bichon\"",
	}

	for _, filter := range filters {
		expr, err := NewExpression(filter)
		if err != nil {
			t.Fatalf("cannot parse '%s': %s", filter, err)
		}

		if expr.Left.Operator != nil {
			t.Fatalf("unexpected unary operator")
		}

		if expr.Left.Match.Project == nil {
			t.Fatalf("missing project match")
		}

		if expr.Left.Match.Project.AsString() != "bichon" {
			t.Fatalf("expected <bichon> got <%s>", expr.Left.Match.Project.AsString())
		}
	}
}

type TestPatternData struct {
	Filter       string
	Value        string
	FlagCaseFold bool
	FlagGlob     bool
}

func TestStringPatterns(t *testing.T) {
	tests := []TestPatternData{
		TestPatternData{
			"~p bichon /i",
			"bichon",
			true,
			false,
		},
		TestPatternData{
			"~p 'bichon' /i",
			"bichon",
			true,
			false,
		},
		TestPatternData{
			"~p \"bichon\" /i",
			"bichon",
			true,
			false,
		},
		TestPatternData{
			"~p bichon /g",
			"bichon",
			false,
			true,
		},
		TestPatternData{
			"~p 'bichon' /i /g",
			"bichon",
			true,
			true,
		},
		TestPatternData{
			"~p \"bichon\" /g /i",
			"bichon",
			true,
			true,
		},
	}

	for _, test := range tests {
		expr, err := NewExpression(test.Filter)
		if err != nil {
			t.Fatalf("cannot parse '%s': %s", test.Filter, err)
		}
		_ = expr.BuildFilter()
		if expr.Left.Operator != nil {
			t.Fatalf("unexpected unary operator")
		}

		if expr.Left.Match.Project == nil {
			t.Fatalf("missing project match")
		}

		if expr.Left.Match.Project.AsString() != test.Value {
			t.Fatalf("expected <%s> got <%s>", test.Value, expr.Left.Match.Project.AsString())
		}

		if expr.Left.Match.Project.HasFlag(PatternFlagCaseFold) != test.FlagCaseFold {
			t.Fatalf("expected casefold %t got %t", test.FlagCaseFold,
				expr.Left.Match.Project.HasFlag(PatternFlagCaseFold))
		}

		if expr.Left.Match.Project.HasFlag(PatternFlagGlob) != test.FlagGlob {
			t.Fatalf("expected glob %t got %t", test.FlagGlob,
				expr.Left.Match.Project.HasFlag(PatternFlagGlob))
		}
	}
}
