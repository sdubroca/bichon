// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package filter

import (
	"fmt"
	"time"

	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer"
	"github.com/alecthomas/participle/v2/lexer/stateful"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
)

type BinaryOperator int

const (
	BinaryOperatorBoth BinaryOperator = iota
	BinaryOperatorEither
)

var binaryOperatorMap = map[string]BinaryOperator{
	"&": BinaryOperatorBoth,
	"|": BinaryOperatorEither,
}

func (o *BinaryOperator) Capture(s []string) error {
	val, ok := binaryOperatorMap[s[0]]
	if !ok {
		return fmt.Errorf("Unknown binary operator '%s'", s[0])
	}
	*o = val
	return nil
}

type UnaryOperator int

const (
	UnaryOperatorReject UnaryOperator = iota
)

var unaryOperatorMap = map[string]UnaryOperator{
	"!": UnaryOperatorReject,
}

func (o *UnaryOperator) Capture(s []string) error {
	val, ok := unaryOperatorMap[s[0]]
	if !ok {
		return fmt.Errorf("Unknown unary operator '%s'", s[0])
	}
	*o = val
	return nil
}

type ComparisonOperator int

const (
	ComparisonOperatorEqual ComparisonOperator = iota
	ComparisonOperatorLess
	ComparisonOperatorLessOrEqual
	ComparisonOperatorGreaterOrEqual
	ComparisonOperatorGreater
)

var comparisonOperatorMap = map[string]ComparisonOperator{
	"==": ComparisonOperatorEqual,
	"<":  ComparisonOperatorLess,
	"<=": ComparisonOperatorLessOrEqual,
	">=": ComparisonOperatorGreaterOrEqual,
	">":  ComparisonOperatorGreater,
}

func (c *ComparisonOperator) Capture(s []string) error {
	val, ok := comparisonOperatorMap[s[0]]
	if !ok {
		return fmt.Errorf("Unknown comparison operator '%s'", s[0])
	}
	*c = val
	return nil
}

type TimeUnit int

const (
	TimeUnitYear TimeUnit = iota
	TimeUnitMonth
	TimeUnitWeek
	TimeUnitDay
	TimeUnitHour
	TimeUnitMinute
	TimeUnitSecond
)

var timeunitMap = map[string]TimeUnit{
	"y": TimeUnitYear,
	"o": TimeUnitMonth,
	"w": TimeUnitWeek,
	"d": TimeUnitDay,
	"h": TimeUnitHour,
	"m": TimeUnitMinute,
	"s": TimeUnitSecond,

	"year":   TimeUnitYear,
	"month":  TimeUnitMonth,
	"week":   TimeUnitWeek,
	"day":    TimeUnitDay,
	"hour":   TimeUnitHour,
	"minute": TimeUnitMinute,
	"second": TimeUnitSecond,

	"years":   TimeUnitYear,
	"months":  TimeUnitMonth,
	"weeks":   TimeUnitWeek,
	"days":    TimeUnitDay,
	"hours":   TimeUnitHour,
	"minutes": TimeUnitMinute,
	"seconds": TimeUnitSecond,

	"mon": TimeUnitMonth,
	"min": TimeUnitMinute,
	"sec": TimeUnitSecond,

	"mons": TimeUnitMonth,
	"mins": TimeUnitMinute,
	"secs": TimeUnitSecond,
}

var timeunitDuration = map[TimeUnit]time.Duration{
	TimeUnitYear:   time.Hour * 24 * 365,
	TimeUnitMonth:  time.Hour * 24 * 31,
	TimeUnitWeek:   time.Hour * 24 * 7,
	TimeUnitDay:    time.Hour * 24,
	TimeUnitHour:   time.Hour,
	TimeUnitMinute: time.Minute,
	TimeUnitSecond: time.Second,
}

func (f *TimeUnit) Capture(str []string) error {
	val, ok := timeunitMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown time unit '%s'", str[0])
	}
	*f = val
	return nil
}

type State int

const (
	StateOpened State = iota
	StateClosed
	StateMerged
	StateLocked
)

var stateMap = map[string]State{
	"o": StateOpened,
	"c": StateClosed,
	"m": StateMerged,
	"l": StateLocked,

	"open":   StateOpened,
	"closed": StateClosed,
	"merged": StateMerged,
	"locked": StateLocked,
}

func (s *State) Capture(str []string) error {
	val, ok := stateMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown state '%s'", str[0])
	}
	*s = val
	return nil
}

func (s *State) AsState() model.MergeReqState {
	switch *s {
	case StateOpened:
		return model.STATE_OPENED
	case StateClosed:
		return model.STATE_CLOSED
	case StateMerged:
		return model.STATE_MERGED
	case StateLocked:
		return model.STATE_LOCKED
	}
	panic("Unexpected state")
}

type Status int

const (
	StatusNew Status = iota
	StatusUpdated
	StatusOld
	StatusRead
)

var statusMap = map[string]Status{
	"n": StatusNew,
	"u": StatusUpdated,
	"o": StatusOld,
	"r": StatusRead,

	"new":     StatusNew,
	"updated": StatusUpdated,
	"old":     StatusOld,
	"read":    StatusRead,
}

func (s *Status) Capture(str []string) error {
	val, ok := statusMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown status '%s'", str[0])
	}
	*s = val
	return nil
}

func (s *Status) AsStatus() model.MergeReqStatus {
	switch *s {
	case StatusNew:
		return model.STATUS_NEW
	case StatusUpdated:
		return model.STATUS_UPDATED
	case StatusOld:
		return model.STATUS_OLD
	case StatusRead:
		return model.STATUS_READ
	}
	panic("Unexpected status")
}

type PatternFlag int

const (
	PatternFlagCaseFold PatternFlag = iota
	PatternFlagGlob
)

var regexFlagMap = map[string]PatternFlag{
	"/i": PatternFlagCaseFold,
	"/g": PatternFlagGlob,
}

func (f *PatternFlag) Capture(str []string) error {
	val, ok := regexFlagMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown regex flag '%s'", str[0])
	}
	*f = val
	return nil
}

//  Expression: Term ( Operator Term)*
//  Term: Match | SubExpression
//  SubExpresion: '(' Expression ')'
//  Match: (Not) Field Value?

type Expression struct {
	Left  *Term           `@@`
	Right []*OperatorTerm `( @@ )*`
}

type OperatorTerm struct {
	Operator BinaryOperator `@("&" | "|")`
	Term     *Term          `@@`
}

type Term struct {
	Operator      *UnaryOperator `@("!")?`
	Match         *Match         `("~" @@`
	SubExpression *Expression    ` | "(" @@ ")")`
}

type ApprovedExpression struct {
	Comparison ComparisonOperator `@("==" | ">" | ">=" | "<" | "<=")?`
	Count      int                `@Int`
}

type Match struct {
	Project          *StringPattern      `"p" @@`
	RealName         *StringPattern      `| "n" @@`
	UserName         *StringPattern      `| "u" @@`
	Age              *Time               `| "a" @@`
	Activity         *Time               `| "t" @@`
	Approved         *ApprovedExpression `| "v" @@`
	ApproverRealName *StringPattern      `| "vn" @@`
	ApproverUserName *StringPattern      `| "vu" @@`
	State            *State              `| "s" @("o" | "c" | "m" | "l" |
                                           "open" | "closed" | "merged" | "locked")`
	Status *Status `| "m" @("n" | "u" | "o" | "r" |
                                "new" | "updated" | "old" | "read")`
	Label *String `| "l" @@`
	WIP   bool    `| @"w"`
}

type String struct {
	Bare         *string `@BareString`
	SingleQuoted *string `| @SingleQuotedString`
	DoubleQuoted *string `| @DoubleQuotedString`
}

func (t *String) AsString() string {
	if t.Bare != nil {
		return *t.Bare
	} else if t.SingleQuoted != nil {
		return (*t.SingleQuoted)[1 : len(*t.SingleQuoted)-1]
	} else if t.DoubleQuoted != nil {
		return (*t.DoubleQuoted)[1 : len(*t.DoubleQuoted)-1]
	}
	panic("missing string data")
}

type StringPattern struct {
	Value *String              `@@`
	Flags []*StringPatternFlag `@@*`
}

type StringPatternFlag struct {
	Flag *PatternFlag `@("/i" | "/g")`
}

func (p *StringPattern) AsString() string {
	return p.Value.AsString()
}

func (p *StringPattern) HasFlag(f PatternFlag) bool {
	for _, flag := range p.Flags {
		if *flag.Flag == f {
			return true
		}
	}
	return false
}

type Time struct {
	Value int      `@Int`
	Unit  TimeUnit `@("y" | "o" | "w" | "d"| "h" | "m" | "s" |
                          "mon" | "min" | "sec" |
                          "mons" | "mins" | "secs" |
                          "year" | "month" | "week" | "day" | "hour" | "minute" | "second" |
                          "years" | "months" | "weeks" | "days" | "hours" | "minutes" | "seconds")`
}

func (t *Time) AsDuration() time.Duration {
	return time.Duration(t.Value) * timeunitDuration[t.Unit]
}

func (m *Match) BuildFilter() model.MergeReqFilter {
	if m.Project != nil {
		return mergeReqFilterNickName(m.Project.AsString(),
			m.Project.HasFlag(PatternFlagGlob),
			m.Project.HasFlag(PatternFlagCaseFold))
	} else if m.RealName != nil {
		return mergeReqFilterSubmitterRealName(m.RealName.AsString(),
			m.RealName.HasFlag(PatternFlagGlob),
			m.RealName.HasFlag(PatternFlagCaseFold))
	} else if m.UserName != nil {
		return mergeReqFilterSubmitterUserName(m.UserName.AsString(),
			m.UserName.HasFlag(PatternFlagGlob),
			m.UserName.HasFlag(PatternFlagCaseFold))
	} else if m.Age != nil {
		return mergeReqFilterAge(m.Age.AsDuration())
	} else if m.Activity != nil {
		return mergeReqFilterActivity(m.Activity.AsDuration())
	} else if m.State != nil {
		return mergeReqFilterState(m.State.AsState())
	} else if m.Status != nil {
		return mergeReqFilterMetadataStatus(m.Status.AsStatus())
	} else if m.Label != nil {
		return mergeReqFilterLabels(m.Label.AsString())
	} else if m.WIP {
		return mergeReqFilterWIP()
	} else if m.Approved != nil {
		return mergeReqFilterApproved(m.Approved.Comparison, m.Approved.Count)
	} else if m.ApproverRealName != nil {
		return mergeReqFilterApproverUserName(m.ApproverRealName.AsString(),
			m.ApproverRealName.HasFlag(PatternFlagGlob),
			m.ApproverRealName.HasFlag(PatternFlagCaseFold))
	} else if m.ApproverUserName != nil {
		return mergeReqFilterApproverUserName(m.ApproverUserName.AsString(),
			m.ApproverUserName.HasFlag(PatternFlagGlob),
			m.ApproverUserName.HasFlag(PatternFlagCaseFold))
	}
	panic("Missing match value")
}

func (t *Term) BuildFilter() model.MergeReqFilter {
	if t.Match != nil {
		if t.Operator != nil && *t.Operator == UnaryOperatorReject {
			return mergeReqFilterReject(t.Match.BuildFilter())
		} else {
			return t.Match.BuildFilter()
		}
	} else if t.SubExpression != nil {
		if t.Operator != nil && *t.Operator == UnaryOperatorReject {
			return mergeReqFilterReject(t.SubExpression.BuildFilter())
		} else {
			return t.SubExpression.BuildFilter()
		}
	}
	panic("Missing term match / subexpression")
}

func (e *Expression) BuildFilter() model.MergeReqFilter {
	left := e.Left.BuildFilter()
	for _, right := range e.Right {
		switch right.Operator {
		case BinaryOperatorEither:
			left = mergeReqFilterEither(left,
				right.Term.BuildFilter())
		case BinaryOperatorBoth:
			left = mergeReqFilterBoth(left,
				right.Term.BuildFilter())
		default:
			panic("Unexpected operator")
		}
	}
	return left
}

func NewExpression(filter string) (*Expression, error) {
	lxr := lexer.Must(stateful.NewSimple([]stateful.Rule{
		{"BareString", `[a-zA-Z-/*.:\[\]?][a-zA-Z0-9-/*.:\[\]?]*`, nil},
		{"DoubleQuotedString", `"[^"]+"`, nil},
		{"SingleQuotedString", `'[^"]+'`, nil},
		{"Int", `[0-9]+`, nil},
		{"Whitespace", `[ \t\n\r]+`, nil},
		{"Operator", `[=<>]+`, nil},
		{"Punct", `[~&|!()]`, nil},
	}))
	p := participle.MustBuild(&Expression{},
		participle.Lexer(lxr),
		participle.Elide("Whitespace"),
	)

	log.Infof("Parsing '%s'", filter)
	expr := &Expression{}
	log.Infof("Parse filter expression '%s'", filter)
	err := p.ParseString("", filter, expr)
	if err != nil {
		log.Infof("Error in file expression: %s", err)
		return nil, err
	}
	return expr, nil
}

type FilterInfo struct {
	Expression string
	Matcher    model.MergeReqFilter
	Error      error
}

func BuildFilter(filterexpr string) FilterInfo {
	expr, err := NewExpression(filterexpr)

	filterinfo := FilterInfo{
		Expression: filterexpr,
		Error:      err,
	}

	if err == nil {
		filterinfo.Matcher = expr.BuildFilter()
	}

	return filterinfo
}
