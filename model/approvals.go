// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package model

type Approvals struct {
	Required   int       `json:"required"`
	Remaining  int       `json:"remaining"`
	ApprovedBy []Account `json:"approvedBy"`
}
