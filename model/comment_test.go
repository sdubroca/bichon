// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package model

import (
	"testing"

	log "github.com/sirupsen/logrus"
)

func TestCommentContextRebase(t *testing.T) {
	mreqJSON := `{
  "id": 17,
  "title": "Comment test",
  "createdAt": "2020-11-10T17:23:20.496Z",
  "updatedAt": "2020-11-12T17:11:23.174Z",
  "submitter": {
    "username": "berrange",
    "realname": "Daniel P. Berrangé"
  },
  "assignee": null,
  "description": "",
  "versions": [
    {
      "index": 1,
      "version": 124221150,
      "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
      "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
      "headHash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
      "patches": [
        {
          "hash": "6096c192b1367ec709ca34c5878f93c0d5ab93ae",
          "title": "Insert some lines",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-10T17:21:41Z",
          "updatedAt": "2020-11-10T17:23:56Z",
          "message": "Insert some lines\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -1,6 +1,9 @@\n This is line one\n This is line two\n This is line three\n+Insert one at line three\n+Insert two at line three\n+Insert three at line three\n This is line four\n This is line five\n This is line six\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "ab4a8ad189f3c38526812da98d505b2d3fa6f57f",
          "title": "Remove some lines",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-10T17:21:54Z",
          "updatedAt": "2020-11-10T17:23:56Z",
          "message": "Remove some lines\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -10,9 +10,6 @@ This is line six\n This is line seven\n This is line eight\n This is line nine\n-This is line ten\n-This is line eleven\n-This is line twelve\n This is line thirteen\n This is line fourteen\n This is line fifteen\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "2f6d10655b47068db2b076cb3abe262ef951ccfe",
          "title": "Insert more lines",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-10T17:22:20Z",
          "updatedAt": "2020-11-10T17:23:56Z",
          "message": "Insert more lines\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -15,6 +15,9 @@ This is line fourteen\n This is line fifteen\n This is line sixteen\n This is line seventeen\n+Insert one at line seventeen\n+Insert two at line seventeen\n+Insert three at line seventeen\n This is line eighteen\n This is line nineteen\n This is line twenty\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "6fc57c5e47d7d835fe86e396d7bc11467d4411d2",
          "title": "Remove some inserted lines",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-10T17:22:30Z",
          "updatedAt": "2020-11-10T17:23:56Z",
          "message": "Remove some inserted lines\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -15,8 +15,6 @@ This is line fourteen\n This is line fifteen\n This is line sixteen\n This is line seventeen\n-Insert one at line seventeen\n-Insert two at line seventeen\n Insert three at line seventeen\n This is line eighteen\n This is line nineteen\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "6ace68b6e8a3e85e8fa4ace3557220dbdcd09f25",
          "title": "Modify lines",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-10T17:22:46Z",
          "updatedAt": "2020-11-10T17:23:56Z",
          "message": "Modify lines\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -9,7 +9,7 @@ This is line five\n This is line six\n This is line seven\n This is line eight\n-This is line nine\n+This is line modified nine\n This is line thirteen\n This is line fourteen\n This is line fifteen\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
          "title": "Modify the same line again",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-10T17:22:57Z",
          "updatedAt": "2020-11-10T17:23:56Z",
          "message": "Modify the same line again\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -9,7 +9,7 @@ This is line five\n This is line six\n This is line seven\n This is line eight\n-This is line modified nine\n+This is line modified nine again\n This is line thirteen\n This is line fourteen\n This is line fifteen\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "79e5781dcb16c6c84a16b1008167e9f57fb3338e",
          "title": "Insert at line one",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-11T11:55:45Z",
          "updatedAt": "2020-11-11T11:55:45Z",
          "message": "Insert at line one\n",
          "diffs": [
            {
              "content": "@@ -1,4 +1,6 @@\n This is line one\n+Insert one at line one\n+Insert two at line one\n This is line two\n This is line three\n Insert one at line three\n",
              "newFile": "commenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "5145eb7be90a887aa2046db3bf9837344a935294",
          "title": "Rename comment test file",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-12T16:30:22Z",
          "updatedAt": "2020-11-12T16:30:22Z",
          "message": "Rename comment test file\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "",
              "newFile": "newcommenttest.txt",
              "oldFile": "commenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": true,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "c99c1c37edd9388c8a4c41db3d8b2f059cb2b52e",
          "title": "Delete lines 14 and 15",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-12T16:30:39Z",
          "updatedAt": "2020-11-12T16:30:48Z",
          "message": "Delete lines 14 and 15\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -13,8 +13,6 @@ This is line seven\n This is line eight\n This is line modified nine again\n This is line thirteen\n-This is line fourteen\n-This is line fifteen\n This is line sixteen\n This is line seventeen\n Insert three at line seventeen\n",
              "newFile": "newcommenttest.txt",
              "oldFile": "newcommenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "c6f898cd7b3eec3bf2732eb2bb931cebd69a747d",
          "title": "insert one at line nineteen",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-12T16:32:55Z",
          "updatedAt": "2020-11-12T16:32:55Z",
          "message": "insert one at line nineteen\n",
          "diffs": [
            {
              "content": "@@ -18,4 +18,5 @@ This is line seventeen\n Insert three at line seventeen\n This is line eighteen\n This is line nineteen\n+Insert one at line nineteen\n This is line twenty\n",
              "newFile": "newcommenttest.txt",
              "oldFile": "newcommenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
          "title": "Rename while adding and removing lines",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-11-12T17:03:46Z",
          "updatedAt": "2020-11-12T17:03:46Z",
          "message": "Rename while adding and removing lines\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -5,8 +5,6 @@ This is line two\n This is line three\n Insert one at line three\n Insert two at line three\n-Insert three at line three\n-This is line four\n This is line five\n This is line six\n This is line seven\n@@ -20,3 +18,4 @@ This is line eighteen\n This is line nineteen\n Insert one at line nineteen\n This is line twenty\n+This is line twenty-one\n",
              "newFile": "newercommenttest.txt",
              "oldFile": "newcommenttest.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": true,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        }
      ],
      "bichonMetadata": {
        "partial": false
      }
    }
  ],
  "threads": [
    {
      "id": "01e4b4d2503775156131b2c93072c5b4d48be95d",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:25:27.335Z",
          "updatedt": "2020-11-10T17:25:27.335Z",
          "description": "Comment on patch 1, second inserted line at line three",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "79e5781dcb16c6c84a16b1008167e9f57fb3338e",
            "newFile": "commenttest.txt",
            "newLine": 7,
            "oldFile": "commenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "910dc432c38a51fa265dbd0d84bfbce1b87416bd",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:25:44.81Z",
          "updatedt": "2020-11-10T17:25:44.81Z",
          "description": "Comment on patch two at removed line eleven",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
            "newFile": "commenttest.txt",
            "newLine": 0,
            "oldFile": "commenttest.txt",
            "oldLine": 11
          }
        }
      ]
    },
    {
      "id": "ea59a2227a81c89e0d5461b8625d4974b61c6294",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:27:09.768Z",
          "updatedt": "2020-11-10T17:27:09.768Z",
          "description": "Comment on third patch, second inserted line at line seventeen",
          "system": false,
          "context": {
            "baseHash": "ab4a8ad189f3c38526812da98d505b2d3fa6f57f",
            "startHash": "ab4a8ad189f3c38526812da98d505b2d3fa6f57f",
            "headHash": "2f6d10655b47068db2b076cb3abe262ef951ccfe",
            "newFile": "commenttest.txt",
            "newLine": 19,
            "oldFile": "commenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "664c0e431d70d0b4a564231e4316f9ad2234ae38",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:27:36.069Z",
          "updatedt": "2020-11-10T17:27:36.069Z",
          "description": "Comment on patch 4 at first removed line at line seventeen",
          "system": false,
          "context": {
            "baseHash": "2f6d10655b47068db2b076cb3abe262ef951ccfe",
            "startHash": "2f6d10655b47068db2b076cb3abe262ef951ccfe",
            "headHash": "6fc57c5e47d7d835fe86e396d7bc11467d4411d2",
            "newFile": "commenttest.txt",
            "newLine": 0,
            "oldFile": "commenttest.txt",
            "oldLine": 18
          }
        }
      ]
    },
    {
      "id": "35dc88171857f19009d9b274106c08a07fc7c3ea",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:28:18.732Z",
          "updatedt": "2020-11-10T17:28:18.732Z",
          "description": "Comment on patch 5 on original line nine",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
            "newFile": "commenttest.txt",
            "newLine": 0,
            "oldFile": "commenttest.txt",
            "oldLine": 9
          }
        }
      ]
    },
    {
      "id": "eaab675a7c21c64e175ad0b6c9c58eb3e1af9f02",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:28:33.014Z",
          "updatedt": "2020-11-10T17:28:33.014Z",
          "description": "Comment on patch 5 at modifiued line nine",
          "system": false,
          "context": {
            "baseHash": "6fc57c5e47d7d835fe86e396d7bc11467d4411d2",
            "startHash": "6fc57c5e47d7d835fe86e396d7bc11467d4411d2",
            "headHash": "6ace68b6e8a3e85e8fa4ace3557220dbdcd09f25",
            "newFile": "commenttest.txt",
            "newLine": 12,
            "oldFile": "commenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "c441d727023ffd9e1d0533e47a71cf0d7652e40b",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:28:53.907Z",
          "updatedt": "2020-11-10T17:28:53.907Z",
          "description": "Comment on ptch 6 at modified nine",
          "system": false,
          "context": {
            "baseHash": "6ace68b6e8a3e85e8fa4ace3557220dbdcd09f25",
            "startHash": "6ace68b6e8a3e85e8fa4ace3557220dbdcd09f25",
            "headHash": "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
            "newFile": "commenttest.txt",
            "newLine": 0,
            "oldFile": "commenttest.txt",
            "oldLine": 12
          }
        }
      ]
    },
    {
      "id": "c12d17143ad334f001e8141aab477db745bbfc05",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-10T17:29:06.9Z",
          "updatedt": "2020-11-10T17:29:06.9Z",
          "description": "Comment on patch 6 at second modification to line nine",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "79e5781dcb16c6c84a16b1008167e9f57fb3338e",
            "newFile": "commenttest.txt",
            "newLine": 14,
            "oldFile": "commenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "036ac456ea8e43adee6ac7e0c2d6a92b21136128",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-11T11:54:56.985Z",
          "updatedt": "2020-11-11T11:54:56.985Z",
          "description": "comment on patch 1, context line two",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "79e5781dcb16c6c84a16b1008167e9f57fb3338e",
            "newFile": "commenttest.txt",
            "newLine": 4,
            "oldFile": "commenttest.txt",
            "oldLine": 2
          }
        }
      ]
    },
    {
      "id": "f3b33c574f8d611b2dcfda6a0da81808504cf69f",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:04:09.434Z",
          "updatedt": "2020-11-12T17:04:09.434Z",
          "description": "Comment on removed line fourteen patch 9",
          "system": false,
          "context": {
            "baseHash": "5145eb7be90a887aa2046db3bf9837344a935294",
            "startHash": "5145eb7be90a887aa2046db3bf9837344a935294",
            "headHash": "c99c1c37edd9388c8a4c41db3d8b2f059cb2b52e",
            "newFile": "newcommenttest.txt",
            "newLine": 0,
            "oldFile": "newcommenttest.txt",
            "oldLine": 17
          }
        }
      ]
    },
    {
      "id": "a0e5342b1357838fcf74493b99c122978ed8ec8a",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:04:39.034Z",
          "updatedt": "2020-11-12T17:04:39.034Z",
          "description": "comment on line added at line nineteen in patch 10",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "c6f898cd7b3eec3bf2732eb2bb931cebd69a747d",
            "newFile": "newcommenttest.txt",
            "newLine": 21,
            "oldFile": "newcommenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "89f165df8bb1864c3600398259cbc61f312de5f8",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:05:29.451Z",
          "updatedt": "2020-11-12T17:05:29.451Z",
          "description": "comment line thirteen pach 9",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "c6f898cd7b3eec3bf2732eb2bb931cebd69a747d",
            "newFile": "newcommenttest.txt",
            "newLine": 15,
            "oldFile": "newcommenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "2510062b2e842a09913c299f5f05ac981378e1df",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:06:19.958Z",
          "updatedt": "2020-11-12T17:06:19.958Z",
          "description": "Comment again on removed line fourteen patch 9",
          "system": false,
          "context": {
            "baseHash": "5145eb7be90a887aa2046db3bf9837344a935294",
            "startHash": "5145eb7be90a887aa2046db3bf9837344a935294",
            "headHash": "c99c1c37edd9388c8a4c41db3d8b2f059cb2b52e",
            "newFile": "newcommenttest.txt",
            "newLine": 0,
            "oldFile": "newcommenttest.txt",
            "oldLine": 16
          }
        }
      ]
    },
    {
      "id": "4926ec6cb0f83983bbb3f3f68acdf78bdd9d2029",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:11:11.892Z",
          "updatedt": "2020-11-12T17:11:11.892Z",
          "description": "Comment removed extra line three at line three during rename",
          "system": false,
          "context": {
            "baseHash": "c6f898cd7b3eec3bf2732eb2bb931cebd69a747d",
            "startHash": "c6f898cd7b3eec3bf2732eb2bb931cebd69a747d",
            "headHash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
            "newFile": "newercommenttest.txt",
            "newLine": 0,
            "oldFile": "newcommenttest.txt",
            "oldLine": 8
          }
        }
      ]
    },
    {
      "id": "8e848217152ebf37a811bfa7b6f5ac302bccb383",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:11:23.153Z",
          "updatedt": "2020-11-12T17:11:23.153Z",
          "description": "Comment line twenty-one during rename",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
            "newFile": "newercommenttest.txt",
            "newLine": 21,
            "oldFile": "newercommenttest.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "e324bb57ff7f280648a85edc0e7d7b285835f746",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel P. Berrangé",
            "email": ""
          },
          "createdAt": "2020-11-12T17:11:48.755Z",
          "updatedt": "2020-11-12T17:11:48.755Z",
          "description": "Comment line twnety-one during rename patch 11",
          "system": false,
          "context": {
            "baseHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "startHash": "fb31496d804dac3f80bb76f7e818709c0ca8b277",
            "headHash": "fe1e12b9bfd416c3d082be55041f3db2d9bfc21b",
            "newFile": "newercommenttest.txt",
            "newLine": 21,
            "oldFile": "newercommenttest.txt",
            "oldLine": 0
          }
        }
      ]
    }
  ],
  "state": "opened",
  "labels": [],
  "upVotes": 0,
  "downVotes": 0,
  "mergeStatus": "can_be_merged",
  "mergeAfterPipeline": false,
  "sourceBranch": "comment-test",
  "targetBranch": "master",
  "bichonMetadata": {
    "partial": false,
    "status": "read"
  }
}`

	mreq, err := NewMergeReqFromJSON([]byte(mreqJSON))
	if err != nil {
		t.Fatalf("Cannot parse mreq json: %s", err)
	}
	expectedPatchContexts := [][]Comment{
		[]Comment{
			Comment{
				Description: "Comment on patch 1, second inserted line at line three",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   5,
					OldFile:   "commenttest.txt",
					OldLine:   0,
				},
			},
			Comment{
				Description: "comment on patch 1, context line two",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   2,
					OldFile:   "commenttest.txt",
					OldLine:   2,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "Comment on patch two at removed line eleven",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   0,
					OldFile:   "commenttest.txt",
					OldLine:   14,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "Comment on third patch, second inserted line at line seventeen",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   19,
					OldFile:   "commenttest.txt",
					OldLine:   0,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "Comment on patch 4 at first removed line at line seventeen",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   0,
					OldFile:   "commenttest.txt",
					OldLine:   18,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "Comment on patch 5 on original line nine",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   0,
					OldFile:   "commenttest.txt",
					OldLine:   12,
				},
			},
			Comment{
				Description: "Comment on patch 5 at modifiued line nine",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   12,
					OldFile:   "commenttest.txt",
					OldLine:   0,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "Comment on ptch 6 at modified nine",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   0,
					OldFile:   "commenttest.txt",
					OldLine:   12,
				},
			},
			Comment{
				Description: "Comment on patch 6 at second modification to line nine",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   12,
					OldFile:   "commenttest.txt",
					OldLine:   0,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "comment on patch 1, context line two",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "commenttest.txt",
					NewLine:   4,
					OldFile:   "commenttest.txt",
					OldLine:   2,
				},
			},
		},
		[]Comment{},
		[]Comment{
			Comment{
				Description: "Comment on removed line fourteen patch 9",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "newcommenttest.txt",
					NewLine:   0,
					OldFile:   "newcommenttest.txt",
					OldLine:   17,
				},
			},
			Comment{
				Description: "Comment again on removed line fourteen patch 9",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "newcommenttest.txt",
					NewLine:   0,
					OldFile:   "newcommenttest.txt",
					OldLine:   16,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "comment on line added at line nineteen in patch 10",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "newcommenttest.txt",
					NewLine:   21,
					OldFile:   "newcommenttest.txt",
					OldLine:   0,
				},
			},
		},
		[]Comment{
			Comment{
				Description: "Comment removed extra line three at line three during rename",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "newercommenttest.txt",
					NewLine:   0,
					OldFile:   "newcommenttest.txt",
					OldLine:   8,
				},
			},
			Comment{
				Description: "Comment line twenty-one during rename",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "newercommenttest.txt",
					NewLine:   21,
					OldFile:   "newercommenttest.txt",
					OldLine:   0,
				},
			},
			Comment{
				Description: "Comment line twnety-one during rename patch 11",
				Context: &CommentContext{
					BaseHash:  "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					StartHash: "fb31496d804dac3f80bb76f7e818709c0ca8b277",
					HeadHash:  "8e3537f0d5465ba2f8cc1d9c8becf5ccc7db5734",
					NewFile:   "newercommenttest.txt",
					NewLine:   21,
					OldFile:   "newercommenttest.txt",
					OldLine:   0,
				},
			},
		},
	}

	series := mreq.Versions[0]

	for idx, patch := range series.Patches {
		actualContext := []Comment{}
		for tidx, thread := range mreq.Threads {
			comment := thread.Comments[0]

			log.Infof("#%d.%d Rebase thread %v", idx+1, tidx+1, comment.Context)
			newCtx := comment.Context.Rebase(&patch, &series)
			if newCtx != nil {
				log.Info("PASS")
				actualContext = append(actualContext, Comment{
					Description: comment.Description,
					Context:     newCtx,
				})
			} else {
				log.Info("FAIL")
			}
		}

		expectContext := expectedPatchContexts[idx]

		if len(expectContext) != len(actualContext) {
			t.Fatalf("#%d Expected %d comments on patch but got %d",
				idx+1, len(expectContext), len(actualContext))
		}

		for cidx, _ := range expectContext {
			c1 := expectContext[cidx]
			c2 := actualContext[cidx]

			if c1.Description != c2.Description {
				t.Fatalf("#%d.%d Expected '%s' got '%s'",
					idx+1, cidx+1, c1.Description, c2.Description)
			}
			a := c1.Context
			b := c2.Context

			if a.NewFile != b.NewFile || a.NewLine != b.NewLine {
				t.Fatalf("#%d.%d Expected new %s:%d context %d patch %d but got %s:%d",
					idx+1, cidx+1, a.NewFile, a.NewLine, cidx, idx, b.NewFile, b.NewLine)
			}
			if a.OldFile != b.OldFile || a.OldLine != b.OldLine {
				t.Fatalf("#%d.%d Expected old %s:%d context %d patch %d but got %s:%d",
					idx+1, cidx+1, a.OldFile, a.OldLine, cidx, idx, b.OldFile, b.OldLine)
			}
		}
	}
}
