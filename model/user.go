// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}
